
    <?php $__env->startSection('content'); ?>
        <div id="content" class="site-content">

            <section class="top-v5">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-6 col-lg-12 align-self-center">
                            <div class="top-left-v5">
                                <div class="ot-heading">
                                    <span>// Welcome to Digital Persona</span>
                                    <h2 class="main-heading">Flexibility, Agility, Customer Centricity</h2>
                                </div>
                                <p class="sub-text">Dedicated to driving digital transformation across industries by leveraging best-of-breed SAP solutions</p>
                                <div class="video-popup style-3">
                                    <div class="btn-inner">
                                        <a class="btn-play" href="#"><i class="flaticon-play"></i>
                                            <span class="circle-1"></span>
                                            <span class="circle-2"></span>
                                        </a>
                                    </div>
                                    <span>video showcase</span>     
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-12">
                            <div class="top-right-v5">
                                <img src="images/front_img.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="service-v5">
                <div class="container">
                    <div class="row mt--70 justify-content-center">
                        <div class="col-lg-4 col-md-6">
                            <div class="serv-box s1 v5 bg-s1 md-mb-30">
                                <a href="#" class="overlay"></a>
                                <div class="icon-main"><span class="flaticon-tablet"></span></div>
                                <div class="content-box">
                                    <h5>SAP Fiori <br>UX</h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <div class="serv-box s1 v5 bg-s2 md-mb-30">
                                <a href="#" class="overlay"></a>
                                <div class="icon-main"><span class="flaticon-report"></span></div>
                                <div class="content-box">
                                    <h5>Mobile Application Development</h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <div class="serv-box s1 v5 bg-s3">
                                <a href="#" class="overlay"></a>
                                <div class="icon-main"><span class="flaticon-ui"></span></div>
                                <div class="content-box">
                                    <h5> Artificial Intelligence <br> (AI)</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="about-v4">
                <div class="overlay overlay-image"></div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-4 mb-4 mb-lg-0">
                                <img src="images/about-us.png" alt="">
                        </div>
                        <div class="col-lg-8">
                            <div class="right-about-v4">
                                <div class="ot-heading">
                                    <span>// about company</span>
                                    <h3 class="main-heading">Your Partner for Software Innovation</h3>
                                </div>
                                <div class="space-5"></div>
                                <p>Digital Personas is recognised as a global leader in solving business challenges with IT solutions.</p>
                                <p><em class="text-dark"><strong>With our spirit of innovation to help enterprises leverage real-time business insights creating increased business advantage, we have evolved our portfolio to a full suite of services in the area of business intelligence, mobile, cloud and support.</strong></em></p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        
     
         

            <section class="highlight-box bg-dark-primary">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <div class="ot-heading">
                                <span class="text-primary-light">// Full lifecycle management</span>
                                <h2 class="main-heading text-white">Explore How Our DP Platform Works</h2>
                            </div>
                            <div class="space-20"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xl-3 col-lg-6 col-md-6 col-sm-12">
                            <div class="serv-box-2 s2">
                                <span class="big-number">01</span>
                                <div class="icon-main"><span class="flaticon-tablet"></span></div>
                                <div class="content-box">
                                    <h5>Design Thinking</h5>
                                    <div>Explore a library of more than 100 pre-built, process-based Fiori apps. Use them as is or customize them.</div>
                                    <a href="#" class="btn-details"><i class="flaticon-right-arrow-1"></i> LEARN MORE</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-6 col-md-6 col-sm-12">
                            <div class="serv-box-2 s2">
                                <span class="big-number">02</span>
                                <div class="icon-main"> <span class="flaticon-ui"></span></div>
                                <div class="content-box">
                                    <h5>Build</h5>
                                    <div>Create applications 80% faster than with our low-code, drag & drop integrated development environment.</div>
                                    <a href="#" class="btn-details"><i class="flaticon-right-arrow-1"></i> LEARN MORE</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-6 col-md-6 col-sm-12">
                            <div class="serv-box-2 s2">
                                <span class="big-number">03</span>
                                <div class="icon-main"><span class="flaticon-shield"></span></div>
                                <div class="content-box">
                                    <h5>Deploy</h5>
                                    <div>Share your custom developed Digital Personas apps or your existing SAP Fiori apps from one location.</div>
                                    <a href="#" class="btn-details"><i class="flaticon-right-arrow-1"></i> LEARN MORE</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-6 col-md-6 col-sm-12">
                            <div class="serv-box-2 s2">
                                <span class="big-number">04</span>
                                <div class="icon-main"><span class="flaticon-report"></span></div>
                                <div class="content-box">
                                    <h5>Operation Control</h5>
                                    <div>Configure authentication options, log on, and secure apps with existing SAP roles and authorizations.</div>
                                    <a href="#" class="btn-details"><i class="flaticon-right-arrow-1"></i> LEARN MORE</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="news-v5">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="ot-heading mb-0">
                                <span>// our recent news (BLOGS or CASE STUDIES)</span>
                                <h2 class="main-heading">Read Our Latest News</h2>
                            </div>
                        </div>
                        <div class="col-md-6 text-right sm-text-left sm-mt-30 align-self-end">
                            <div class="ot-button">
                                <a href="blog.html" class="octf-btn octf-btn-primary">All News</a>
                            </div>
                            <div class="space-10"></div>
                        </div>
                    </div>
                    <div class="space-40"></div>
                    <div class="post-grid pgrid">
                        <div class="row justify-content-center">
                            <div class="col-lg-4 col-md-6 col-sm-12">
                                <article class="post-box blog-item">
                                    <div class="post-inner">
                                        <div class="entry-media">
                                            <div class="post-cat">
                                                <span class="posted-in">
                                                    <a href="#" rel="category tag">Design</a> 
                                                    <a href="#" rel="category tag">Development</a>
                                                </span>
                                            </div>                                    
                                            <a href="post.html"><img src="https://via.placeholder.com/601x520.png" alt=""></a>
                                        </div>                         
                                        <div class="inner-post">
                                            <div class="entry-header">
                                                <div class="entry-meta">
                                                    <span class="posted-on">_ <a href="#">November 21, 2019</a></span>
                                                    <span class="byline">_ <a class="url fn n" href="#">Tom Black</a></span>
                                                    <span class="comment-num">_ <a href="#">3 Comments</a></span>
                                                </div><!-- .entry-meta -->
                                                
                                                <h3 class="entry-title"><a href="post.html">Plan Your Project  with Your Software</a></h3>
                                            </div><!-- .entry-header -->
                                            
                                            <div class="btn-readmore">
                                                <a href="post.html"><i class="flaticon-right-arrow-1"></i>LEARN MORE</a>
                                            </div>
                                        </div>
                                    </div>
                                </article>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-12">
                                <article class="post-box blog-item">
                                    <div class="post-inner">
                                        <div class="entry-media">
                                            <div class="post-cat">
                                                <span class="posted-in">
                                                    <a href="#" rel="category tag">Design</a> 
                                                    <a href="#" rel="category tag">Development</a>
                                                </span>
                                            </div>                                    
                                            <a href="post.html">
                                                <img src="https://via.placeholder.com/601x520.png" alt="">
                                            </a>
                                        </div>                         
                                        <div class="inner-post">
                                            <div class="entry-header">
                                                <div class="entry-meta">
                                                    <span class="posted-on">_ <a href="#">November 21, 2019</a></span>
                                                    <span class="byline">_ <a class="url fn n" href="#">Tom Black</a></span>
                                                    <span class="comment-num">_ <a href="#">0 Comments</a></span>
                                                </div><!-- .entry-meta -->
                                                
                                                <h3 class="entry-title"><a href="post.html">You have a Great  Business Idea?</a></h3>
                                            </div><!-- .entry-header -->
                                            
                                            <div class="btn-readmore">
                                                <a href="post.html"><i class="flaticon-right-arrow-1"></i>LEARN MORE</a>
                                            </div>
                                        </div>
                                    </div>
                                </article>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-12">
                                <article class="post-box blog-item">
                                    <div class="post-inner">
                                        <div class="entry-media">
                                            <div class="post-cat"><span class="posted-in"><a href="#" rel="category tag">Development</a></span></div>
                                            <a href="post.html">
                                                <img src="https://via.placeholder.com/601x520.png" alt="">
                                            </a>
                                        </div>                         
                                        <div class="inner-post">
                                            <div class="entry-header">
                                                <div class="entry-meta">
                                                    <span class="posted-on">_ <a href="#">September 24, 2019</a></span>
                                                    <span class="byline">_ <a class="url fn n" href="#">Tom Black</a></span>
                                                    <span class="comment-num">_ <a href="#">3 Comments</a></span>
                                                </div><!-- .entry-meta -->
                                                
                                                <h3 class="entry-title"><a href="post.html">Building Data Analytics  Software</a></h3>
                                            </div><!-- .entry-header -->
                                            
                                            <div class="btn-readmore">
                                                <a href="post.html"><i class="flaticon-right-arrow-1"></i>LEARN MORE</a>
                                            </div>
                                        </div>
                                    </div>
                                </article>
                            </div>
                        </div>

                    </div>
                </div>
            </section>
            

            <section class="clients-v5">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 text-center">
                            <div class="ot-heading text-center">
                                <span>// our clients</span>
                                <h2 class="main-heading">We are Trusted <br>15+ Countries Worldwide</h2>
                            </div>
                            <div class="space-35"></div>
                        </div>
                        <div class="col-lg-12">
                            <div class="ot-testimonials">
                                <div class="owl-carousel owl-theme testimonial-inner ot-testimonials-slider">
                                    <div class="testi-item">
                                        <div class="layer1"></div>
                                        <div class="layer2">
                                            <div class="t-head flex-middle">
                                                <img src="https://via.placeholder.com/90x90.png" alt="Emilia Clarke" class="lazyloaded" data-ll-status="loaded">
                                                    <div class="tinfo">
                                                        <h6>SoftTech,</h6>
                                                        <span>Manager of Company</span>                             
                                                    </div>
                                            </div>
                                            <div class="ttext">
                                                "Patience. Infinite patience. No shortcuts. Very well thought out and articulate communication. Clear milestones, deadlines and fast work. Even if the client is being careless. The best part...always solving problems with great original ideas!."                           
                                            </div>
                                        </div>
                                    </div>
                                    <div class="testi-item">
                                        <div class="layer1"></div>
                                        <div class="layer2">
                                            <div class="t-head flex-middle">
                                                <img src="https://via.placeholder.com/90x90.png" alt="Emilia Clarke" class="lazyloaded" data-ll-status="loaded">
                                                    <div class="tinfo">
                                                        <h6>Moonkle LTD,</h6>
                                                        <span>Client of Company</span>                             
                                                    </div>
                                            </div>
                                            <div class="ttext">
                                                "Very well thought out and articulate communication. Clear milestones, deadlines and fast work. Patience. Infinite patience. No shortcuts. Even if the client is being careless. The best part...always solving problems with great original ideas!."                           
                                            </div>
                                        </div>
                                    </div>
                                    <div class="testi-item">
                                        <div class="layer1"></div>
                                        <div class="layer2">
                                            <div class="t-head flex-middle">
                                                <img src="https://via.placeholder.com/90x90.png" alt="Emilia Clarke" class="lazyloaded" data-ll-status="loaded">
                                                    <div class="tinfo">
                                                        <h6>SoftTech,</h6>
                                                        <span>Manager of Company</span>                             
                                                    </div>
                                            </div>
                                            <div class="ttext">
                                                "Patience. Infinite patience. No shortcuts. Very well thought out and articulate communication. Clear milestones, deadlines and fast work. Even if the client is being careless. The best part...always solving problems with great original ideas!."                           
                                            </div>
                                        </div>
                                    </div>
                                    <div class="testi-item">
                                        <div class="layer1"></div>
                                        <div class="layer2">
                                            <div class="t-head flex-middle">
                                                <img src="https://via.placeholder.com/90x90.png" alt="Emilia Clarke" class="lazyloaded" data-ll-status="loaded">
                                                    <div class="tinfo">
                                                        <h6>Moonkle LTD,</h6>
                                                        <span>Client of Company</span>                             
                                                    </div>
                                            </div>
                                            <div class="ttext">
                                                "Very well thought out and articulate communication. Clear milestones, deadlines and fast work. Patience. Infinite patience. No shortcuts. Even if the client is being careless. The best part...always solving problems with great original ideas!."                           
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            

        </div>
        <?php $__env->stopSection(); ?>

<?php echo $__env->make('layout.layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\Laravel Projects\history_master\resources\views/web/index.blade.php ENDPATH**/ ?>