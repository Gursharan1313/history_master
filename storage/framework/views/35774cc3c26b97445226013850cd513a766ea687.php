

<?php $__env->startSection('body'); ?>
    <body class="login">
        <?php echo $__env->yieldContent('content'); ?>
        <!-- BEGIN: JS Assets-->
        <script src="<?php echo e(mix('dist/js/app.js')); ?>"></script>
        <!-- END: JS Assets-->

        <?php echo $__env->yieldContent('script'); ?>
    </body>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('../layout/base', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\Laravel Projects\admin theme\dp-1\resources\views////layout/login.blade.php ENDPATH**/ ?>