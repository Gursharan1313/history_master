<?php $__env->startSection('subhead'); ?>
<title><?php echo e($pageName); ?> - Digital Personas</title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('subcontent'); ?>
    <div class="grid grid-cols-12 gap-6">
        <div class="col-span-12">
            <div class="grid grid-cols-12 gap-6">

                <!-- BEGIN: Weekly Top Products -->
                <div class="col-span-12 text-center mt-6">
                    <div class="wlc text-center">
                       <h1>Welcome to Digital Personas Dashboard</h1>
                    </div>
                    <div class="inline-flex">
                        <img width="450" src="<?php echo e(asset('dist/imgs/dashboard.png')); ?>">
                    </div>
                </div>
                <!-- END: Weekly Top Products -->
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('../layout/' . $layout, \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\laravel project\git dp\dp\resources\views/pages/dashboard.blade.php ENDPATH**/ ?>