<!DOCTYPE html>
<html>
<!-- BEGIN: Head -->
<head>
    <meta charset="utf-8">
    <link href="<?php echo e(asset('dist/imgs/favicon.png')); ?>" rel="shortcut icon">
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">

    <?php if(!empty($pageName)): ?>
        <title><?php echo e($pageName." - Digital Personas"); ?></title>
     <?php else: ?>
          <title> <?php echo e("Dashboard - Digital Personas"); ?></title>    
    <?php endif; ?>

    <!-- BEGIN: CSS Assets-->
    <link rel="stylesheet" href="<?php echo e(mix('dist/css/app.css')); ?>" />
    <link rel="stylesheet" href="<?php echo e(asset('dist/css/app1.css')); ?>" />
    <link rel="stylesheet" href="<?php echo e(asset('dist/css/app2.css')); ?>" />
    <link rel="stylesheet" href="<?php echo e(asset('dist/css/app3.css')); ?>" />
    <link rel="stylesheet" href="<?php echo e(asset('dist/css/app4.css')); ?>" />
    <link rel="stylesheet" href="<?php echo e(asset('dist/css/custom.css')); ?>" />
 
    <!-- END: CSS Assets-->
</head>
<!-- END: Head -->

<?php echo $__env->yieldContent('body'); ?>

</html><?php /**PATH E:\Laravel Projects\admin theme\dp-1\resources\views////layout/base.blade.php ENDPATH**/ ?>