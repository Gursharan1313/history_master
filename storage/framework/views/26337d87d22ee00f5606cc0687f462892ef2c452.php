
    <?php $__env->startSection('content'); ?>
    <div id="content" class="site-content">
        
        <div class="container">
            <div class="center-text pt-50 not-found text-center">
                <h2><img class="error-image" src="images/404-error.png" alt="404"></h2>
                <h1>Sorry! Page Not Found!</h1>
                <div class="content-404">
                    <p>Oops! The page you are looking for does not exist. Please return to the site is homepage.</p>
                    <a class="octf-btn" href="/">Take Me Home</a>
                </div>
            </div>
        </div>
        
    </div>
    <?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\Laravel Projects\history_master\resources\views/errors/404.blade.php ENDPATH**/ ?>