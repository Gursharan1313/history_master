

<?php $__env->startSection('subcontent'); ?>
    <div class="intro-y flex flex-col sm:flex-row items-center mt-8">
        <h2 class="text-lg font-medium mr-auto">Edit Tag & Categories</h2>
        <div class="w-full sm:w-auto flex mt-4 sm:mt-0">
            <button data-id="addNewCate" class="addNewCate btn popup btn-primary shadow-md mr-2">Add New</button>
        </div>
    </div>
    <div id="addNewCate" class="intro-y box p-5 mt-5">
        <div class="overflow-x-auto scrollbar-hidden md:col-span-6 lg:col-span-8">
            <form id="cate" class="form" action="<?php echo e(route('category/submit')); ?>"> 
                <?php echo csrf_field(); ?>
                <div class="form-group mb-3">
                    <label>Name:</label>
                    <input name="name" id="catename" class="form-control" placeholder="name" required>
                </div>
                <div class="form-group mb-3">
                    <label>Type:</label>
                    <select class="form-control" id="catetype" name="type" required>
                        <option hidden>Select one</option>
                        <option value="category">Category</option>
                        <option value="tag">Tag</option>
                    </select>
                </div>
                <div class="form-group mb-3">
                    <label>Status:</label>
                    <select class="form-control" id="catestatus" name="status" required>
                        <option value="enable">Enable</option>
                        <option value="disable">Disable</option>
                    </select>
                </div>
                 <div class="form-group mb-3">
                    <input id="submit" class="btn btn-primary " type="submit">
                    <span id="update" data-id="" class="btn btn-warning hidden">update</span>
                </div>
            </form>
        </div>
    </div>
    <!-- BEGIN: HTML Table Data -->
    <div class="intro-y box p-5 mt-5">
        <div class="overflow-x-auto scrollbar-hidden md:col-span-6 lg:col-span-8">
            <div class="mt-5 table-report table-report--tabulator">
                    <table class="table table-hover table-bordered">
                        <thead>
                        <tr>
                            <th>Sno.</th>
                            <th>Name</th>
                            <th>Type</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php 
                                $sno = '1';
                            ?>
                            <?php $__currentLoopData = $cat; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $field): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td><?php echo e($sno); ?></td>
                                    <td><?php echo e($field->name); ?></td>
                                    <td><?php echo e($field->type); ?></td>
                                    <td><?php echo e($field->status); ?></td>
                                    <td>
                                        <button data-id="<?php echo e($field->id); ?>" data-name="<?php echo e($field->name); ?>" data-type="<?php echo e($field->type); ?>" data-st="<?php echo e($field->status); ?>" class="btn btn-success cateEdit">Edit</button>
                                        <button data-id="<?php echo e($field->id); ?>" class="btn btn-danger cateDel">Delete</button>
                                    </td>
                                </tr>
                                <?php 
                                $sno++;
                            ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    </table>
            </div>
        </div>
    </div>
    <!-- END: HTML Table Data -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('../layout/' . $layout, \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\Laravel Projects\history_master\resources\views/pages/tabulator.blade.php ENDPATH**/ ?>