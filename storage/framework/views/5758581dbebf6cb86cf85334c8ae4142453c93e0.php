<?php $__env->startSection('subcontent'); ?>
    <div class="intro-y flex items-center mt-8">
        <h1 class="text-lg font-medium mr-5"><?php echo e($pageName); ?></h1>
        <button id="addNewMenu" data-id="AddMenu" data-animate="fade" class="addNewMenu popup btn btn-block btn-elevated-primary mr-auto">Add new</button>
    </div>
    <div class="grid grid-cols-12 gap-6 mt-5">
    <?php $__currentLoopData = $menu; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $menu): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <div class="intro-y col-span-12 lg:col-span-4">
            <!-- BEGIN: Multi Select -->
            <div class="intro-y box">
                <div class="flex flex-col sm:flex-row items-center p-5 border-b border-gray-200 dark:border-dark-5">
                    <h2 class="font-medium text-base mr-auto"><?php echo e($menu->menu_name); ?></h2>
                    <button class="btn btn-block btn-danger delMenu" data-id="<?php echo e($menu->id); ?>">Delete</button>
                </div>
                <div id="multi-select" class="p-5">
                  <form method="post" class="menu" action="updateMenu" id="updateMenu">  
                  <?php echo csrf_field(); ?>
                  <input type="hidden" name="menu_id" value="<?php echo e($menu->id); ?>">
                    <div class="form-group">
                        <label>Rename: </label>
                        <input type="text" name="rename" placeholder="Rename menu name" value="<?php echo e($menu->menu_name); ?>" class="form-control mb-3">
                    </div>
                    <div class="preview">
                        <label>Add Sub Menu:</label>
                        <select data-placeholder="Select Sub Menu Page" name='sub-menu[]' data-search="true" class="tail-select w-full" multiple>
                        <?php $__currentLoopData = $menu_items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($item->page_title); ?>"><?php echo e($item->page_title); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Change Position: </label>
                        <div class="mt-2">
                            <select data-search="true" name="position"  class="tail-select w-full">
                                <option value="header">Header</option>
                                <option value="footer">Footer</option>
                                <option value="both">Both</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group mt-2">
                        <input type="submit" name="update" class="btn btn-elevated-dark mr-auto" value="update">
                    </div>
                    </form>
                </div>
            </div>
            <!-- END: Multi Select -->
        </div>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </div>  

    <div id="AddMenu">
        <div data-animate="fade" class="close"></div>
        <div class="Addmenu"> 
            <form method="post" class="form" action="addMenu" id="addMenu">
            <?php echo csrf_field(); ?>
                <!-- BEGIN: Basic Select -->
                <div class="intro-y box">
                    <div class="flex flex-col sm:flex-row items-center p-5 border-b border-gray-200 dark:border-dark-5">
                        <h2 class="font-medium text-base mr-auto">Add New Menu</h2>
                    </div>
                    <div id="basic-select" class="p-5">
                        <div class="preview">
                            <!-- BEGIN: Basic Select -->
                            <div>
                                <label>Page Name</label>
                                <div class="mt-2">
                                    <select name="menu_name" data-search="true" class="tail-select w-full">
                                        <?php $__currentLoopData = $menu_items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($item->page_title); ?>"><?php echo e($item->page_title); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                </div>
                            </div>
                            <!-- END: Basic Select -->
                        </div>
                    </div>
                    <div class="text-center">
                        <p>or</p>
                    </div>
                    <div class="form-group p-5 pt-0"> 
                        <label class="form-label">Link:</label>
                        <input name="link" class="form-control mt-2" type="text" placeholder="Link"> 
                    </div> 
                    <div class="form-group p-5 pt-0"> 
                        <label class="form-label">Rename:</label>
                        <input name="rename" class="form-control mt-2" type="text" placeholder="Rename"> 
                    </div> 
                    <div class="form-group p-5 pt-0"> 
                        <div class="preview">
                            <!-- BEGIN: Basic Select -->
                            <div>
                                <label>Menu Position:</label>
                                <div class="mt-2">
                                    <select data-search="true" name="position" class="tail-select w-full" required>
                                        <option value="header">Header</option>
                                        <option value="footer">Footer</option>
                                        <option value="both">Both</option>
                                    </select>
                                </div>
                            </div>
                            <!-- END: Basic Select -->
                        </div>
                    </div> 
                    <div class="form-group p-5 pt-0"> 
                        <input class="form-control btn-submit mt-2 btn-elevated-warning" type="submit" name="submit"> 
                    </div>
                </div>
                <!-- END: Basic Select -->
            </form>
        </div>
    </div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('../layout/' . $layout, \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\laravel project\git dp\dp\resources\views/pages/menu.blade.php ENDPATH**/ ?>