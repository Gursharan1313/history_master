<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <title>Digital Persona | One For Complete Software Solutions</title>
    <link rel="shortcut icon" type="image/x-icon" href="images/favicon.png" />
    <link rel="stylesheet" href="<?php echo e(asset('dist/web/css/bootstrap.min.css')); ?>" />
    <link rel="stylesheet" href="<?php echo e(asset('dist/web/css/font-awesome.min.css')); ?>" />
    <link rel="stylesheet" href="<?php echo e(asset('dist/web/css/flaticon.css')); ?>" />
    <link rel="stylesheet" href="<?php echo e(asset('dist/web/css/flaticon2.css')); ?>" />
    <link rel="stylesheet" href="<?php echo e(asset('dist/web/css/owl.carousel.min.css')); ?>" />
    <link rel="stylesheet" href="<?php echo e(asset('dist/web/css/owl.theme.css')); ?>" />
    <link rel="stylesheet" href="<?php echo e(asset('dist/web/css/magnific-popup.css')); ?>" />
    <link rel="stylesheet" href="<?php echo e(asset('dist/web/plugins/aos-master/dist/aos.css')); ?>" />
    <link rel="stylesheet" href="<?php echo e(asset('dist/web/style.css')); ?>" />
    <link rel="stylesheet" href="<?php echo e(asset('dist/web/css/royal-preload.css')); ?>" />
</head>

<body class="royal_preloader">
    <div id="page" class="site">
        <header id="site-header" class="site-header sticky-header header-static">
            <!-- Main Header start -->
            <div class="header-topbar style-2">
                <div class="octf-area-wrap">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4">
                                <ul class="social-list">
                                    <li><a href="http://twitter.com" target="_self"><i class="fab fa-twitter"></i></a></li>
                                    <li><a href="http://facebook.com" target="_self"><i class="fab fa-facebook-f"></i></a></li>
                                    <li><a href="http://linkedin.com" target="_self"><i class="fab fa-linkedin-in"></i></a></li>
                                    <li><a href="http://instagram" target="_self"><i class="fab fa-instagram"></i></a></li>
                                </ul>
                            </div>
                            <div class="col-md-8">
                                <ul class="topbar-info align-self-end clearfix">
                                    <li><a href="tel:040-4018-1212"><i class="fas fa-phone-alt"></i> 040-4018-1212</a></li>
                                    <li><a href="mailto:info@digitalpersonas.com"><i class="fas fa-envelope"></i> info@digitalpersonas.com</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="octf-main-header">
                <div class="octf-area-wrap">
                    <div class="container octf-mainbar-container">
                        <div class="octf-mainbar">
                            <div class="octf-mainbar-row octf-row">
                                <div class="octf-col logo-col">
                                    <div id="site-logo" class="site-logo">
                                        <a href="/">
                                            <img src="images/logo.svg" alt="Engitech" class="">
                                        </a>
                                    </div>
                                </div>
                                <div class="octf-col menu-col">
                                    <nav id="site-navigation" class="main-navigation">
                                        <ul class="menu">
                                            <li class="current-menu-item current-menu-ancestor">
                                                <a href="/">Home</a>
                                            </li>
                                            <li class="menu-item-has-children"><a href="#">Services</a>
                                            <ul id="sap" class="sub-menu">
                                                    <li class="menu-item-has-children"><a href="#">SAP Services</a>
                                                        <ul class="sub-menu">
                                                            <li><a href="">SAP Fiori</a></li>
                                                            <li><a href="#">SAP HANA</a></li>
                                                            <li><a href="#">SAP Cloud</a></li>
                                                            <li><a href="#">SAP Implementations</a></li>
                                                            <li><a href="#">Enterprise Mobility 		</a></li>
                                                        </ul>
                                                    </li>
                                                    <li class="menu-item-has-children"><a href="#">Hybrid/Web Mobile Development</a>
                                                        <ul class="sub-menu">
                                                            <li><a href="#">Native Development</a></li>
                                                            <li><a href="#">Hybrid Development</a></li>
                                                            <li><a href="#">Design Thinking & Strategy</a></li>
                                                            <li><a href="#">Progressive Web Development</a></li>
                                                            <li><a href="#">Full Stack Development</a></li>
                                                            <li><a href="#">Enterprise Mobile App Development</a></li>
                                                            <li><a href="#">UX Design services</a></li>
                                                        </ul>
                                                    </li>
                                                    <li><a href="#">Strategic Consulting Services</a></li>
                                                    <li><a href="">Staff Augmentation</a></li>
                                                </ul>
                                            </li>
                                            <li class="menu-item-has-children"><a href="#">Solutions</a>
                                                <ul class="sub-menu">
                                                    <li><a href="#">HR & Recruitment</a></li>
                                                    <li><a href="#">Digital Gaming</a></li>
                                                    <li><a href="#">Digital ERP</a></li>
                                                    <li><a href="#">SAP 100+ Custom Fiori Apps</a></li>
                                                </ul>
                                            </li>
                                            <li class="menu-item-has-children"><a href="#">Technology</a>
                                            <ul class="sub-menu">
                                                    <li><a href="portfolio">All in Our ppt image</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="case_study">Case Studies</a></li>
                                            <li><a href="contact">Reach Us</a></li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="header_mobile">
                <div class="container">
                    <div class="mlogo_wrapper clearfix">
                        <div class="mobile_logo">
                            <a href="index.html">
                                <img src="images/logo.svg" alt="Engitech">
                            </a>
                        </div>
                        <div id="mmenu_toggle">
                            <button></button>
                        </div>
                    </div>
                    <div class="mmenu_wrapper">
                        <div class="mobile_nav collapse">
                            <ul id="menu-main-menu" class="mobile_mainmenu">
                            <li class="current-menu-item current-menu-ancestor">
                                                <a href="index.php">Home</a>
                                            </li>
                                            <li class="menu-item-has-children"><a href="#">Services</a>
                                            <ul id="sap" class="sub-menu">
                                                    <li class="menu-item-has-children"><a href="#">SAP Services</a>
                                                        <ul class="sub-menu">
                                                            <li><a href="sapfoiri.php">SAP Fiori</a></li>
                                                            <li><a href="#">SAP HANA</a></li>
                                                            <li><a href="#">SAP Cloud</a></li>
                                                            <li><a href="#">SAP Implementations</a></li>
                                                            <li><a href="#">Enterprise Mobility 		</a></li>
                                                        </ul>
                                                    </li>
                                                    <li class="menu-item-has-children"><a href="#">Hybrid/Web Mobile Development</a>
                                                        <ul class="sub-menu">
                                                            <li><a href="#">Native Development</a></li>
                                                            <li><a href="#">Hybrid Development</a></li>
                                                            <li><a href="#">Design Thinking & Strategy</a></li>
                                                            <li><a href="#">Progressive Web Development</a></li>
                                                            <li><a href="#">Full Stack Development</a></li>
                                                            <li><a href="#">Enterprise Mobile App Development</a></li>
                                                            <li><a href="#">UX Design services</a></li>
                                                        </ul>
                                                    </li>
                                                    <li><a href="#">Strategic Consulting Services</a></li>
                                                </ul>
                                            </li>
                                            <li class="menu-item-has-children"><a href="#">Solutions</a>
                                                <ul class="sub-menu">
                                                    <li><a href="#">HR & Recruitment</a></li>
                                                    <li><a href="#">Digital Gaming</a></li>
                                                    <li><a href="#">Digital ERP</a></li>
                                                    <li><a href="#">SAP 100+ Custom Fiori Apps</a></li>
                                                </ul>
                                            </li>
                                            <li class="menu-item-has-children"><a href="#">technology</a>
                                            <ul class="sub-menu">
                                                    <li><a href="portfolio.php">All in Our ppt image</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="#">Case Studies</a></li>
                                            <li><a href="contact.php">Reach Us</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <?php echo $__env->yieldContent('content'); ?>;

        
        <div class="padding-half bg-light-2">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="partners">
                            <div class="owl-carousel owl-theme home-client-carousel">

                                <div class="partners-slide">
                                    <a href="#" class="client-logo">
                                        <figure class="partners-slide-inner">
                                            <img class="partners-slide-image" src="images/carosel/c1.png" alt="">
                                        </figure>                             
                                    </a>
                                </div>
                                <div class="partners-slide">
                                    <a href="#" class="client-logo">
                                        <figure class="partners-slide-inner">
                                            <img class="partners-slide-image" src="images/carosel/c2.png" alt="">
                                        </figure>                             
                                    </a>
                                </div>
                                <div class="partners-slide">
                                    <a href="#" class="client-logo">
                                        <figure class="partners-slide-inner">
                                            <img class="partners-slide-image" src="images/carosel/c3.png" alt="">
                                        </figure>                             
                                    </a>
                                </div>
                                <div class="partners-slide">
                                    <a href="#" class="client-logo">
                                        <figure class="partners-slide-inner">
                                            <img class="partners-slide-image" src="images/carosel/c4.png" alt="">
                                        </figure>                             
                                    </a>
                                </div>
                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <footer id="site-footer" class="site-footer footer-v1">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                    <div class="widget-footer">
                        <h5 class="text-white">Services</h5>
                        <ul class="list-items">
                            <li class="list-item"><a href="#">SAP Fiori</a></li>
                            <li class="list-item"><a href="#">Blockchain</a></li>
                            <li class="list-item"><a href="#">Mobile App Development</a></li>
                            <li class="list-item"><a href="#">SAP Lumira</a></li>
                            <li class="list-item"><a href="#">IT Staff Augmentation</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                    <div class="widget-footer">
                        <h5 class="text-white">IT Solutions</h5>
                        <ul class="list-items">
                            <li class="list-item"><a href="#">CLoud Solutions</a></li>
                            <li class="list-item"><a href="#">CRM Solutions</a></li>
                            <li class="list-item"><a href="#">SRM Solutions</a></li>
                            <li class="list-item"><a href="#">Enterprise Management Solution</a></li>
                            <li class="list-item"><a href="#">Rapid Deployment Solutions</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                    <div class="widget-footer">
                        <h5 class="text-white">Company</h5>
                        <ul class="list-items">
                            <li class="list-item"><a href="#">About Company</a></li>
                            <li class="list-item"><a href="#">For Customers</a></li>
                            <li class="list-item"><a href="#">Blog & News</a></li>
                            <li class="list-item"><a href="#">Careers & Reviews</a></li>
                            <li class="list-item"><a href="#">Sitemap</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                    <div class="widget-footer">
                        <h5 class="text-white">Subscribe</h5>
                        <p>Follow our newsletter to stay updated  about agency.</p>
                        <form action="newsletter.php" id="mc4wp-form-1" class="mc4wp-form" method="post">
                            <div class="mc4wp-form-fields">
                                <div class="subscribe-inner-form">
                                    <input type="email" name="email" placeholder="Your Email" required="">
                                    <button type="submit" class="subscribe-btn-icon"><i class="flaticon-telegram"></i></button>
                                </div>  
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="row mt-35">
                <div class="col-md-4 mb-4 mb-md-0">
                    <img src="images/logo.png" width="150px" alt="" class="lazyloaded" data-ll-status="loaded">
                </div>
                <div class="col-md-8 text-left text-md-right align-self-center">
                    <p class="copyright-text">Copyright © 2021 Digital Persona by <a href="http://designforrank.com" target="_blank">Design For Rank</a>. All Rights Reserved.</p>
                </div>
            </div>
        </div>
    </footer><!-- #site-footer -->
</div><!-- #page -->
<a id="back-to-top" href="#" class="show"><i class="flaticon-up-arrow"></i></a>
        <!-- jQuery -->
    <script src="<?php echo e(asset('dist/web/js/jquery.min.js')); ?>"></script>
    <script src="<?php echo e(asset('dist/web/js/jquery.magnific-popup.min.js')); ?>"></script>
    <script src="<?php echo e(asset('dist/web/js/jquery.isotope.min.js')); ?>"></script>
    <script src="<?php echo e(asset('dist/web/js/owl.carousel.min.js')); ?>"></script>
    <script src="<?php echo e(asset('dist/web/js/easypiechart.min.js')); ?>"></script>
    <script src="<?php echo e(asset('dist/web/js/jquery.countdown.min.js')); ?>"></script>
    <script src="<?php echo e(asset('dist/web/js/scripts.js')); ?>"></script>
    <script src="<?php echo e(asset('dist/web/js/header-mobile.js')); ?>"></script>
    <script src="<?php echo e(asset('dist/web/js/royal_preloader.min.js')); ?>"></script>
    <script>
        window.jQuery = window.$ = jQuery;  
        (function($) { "use strict";
            //Preloader
            Royal_Preloader.config({
                mode           : 'logo',
                logo           : "<?php echo e(asset('dist/web/images/logo.svg')); ?>",
                logo_size      : [160, 75],
                showProgress   : true,
                showPercentage : true,
                text_colour: '#000000',
                background:  '#ffffff'
            });
        })(jQuery);
    </script> 

   <script src="<?php echo e(asset('dist/web/plugins/aos-master/dist/aos.js')); ?>"></script>
    <script>
      AOS.init({
        easing: 'ease-in-out-sine'
      });
    </script>
</body>
</html><?php /**PATH E:\Laravel Projects\admin theme\dp-1\resources\views/layout/layout.blade.php ENDPATH**/ ?>