

<?php $__env->startSection('subcontent'); ?>
    <div class="intro-y flex flex-col sm:flex-row items-center mt-8">
        <h2 class="text-lg font-medium mr-auto">Site Info</h2>
        <div class="w-full sm:w-auto flex mt-4 sm:mt-0">
           <!-- <button type="button" class="btn box text-gray-700 dark:text-gray-300 mr-2 flex items-center ml-auto sm:ml-0">
                <i class="w-4 h-4 mr-2" data-feather="eye"></i> Preview
            </button>-->
        </div>
        <?php if(Session::has('msg')): ?>
            <div class="alert alert-success alert-dismissible show flex items-center mb-5" role="alert"> <i data-feather="alert-triangle" class="w-6 h-6 mr-2"></i> <?php echo e(Session::get('msg')); ?> <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"> <i data-feather="x" class="w-4 h-4"></i> </button> </div>
        <?php endif; ?>
        <?php if(Session::has('error')): ?>
        <div class="alert alert-danger alert-dismissible show flex items-center mb-2" role="alert"> <i data-feather="alert-octagon" class="w-6 h-6 mr-2"></i>  <?php echo e(Session::get('error')); ?> <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"> <i data-feather="x" class="w-4 h-4"></i> </button> </div>
        <?php endif; ?>
    </div>
       
   <form id="siteData" method="post" action="siteInfoData" enctype="multipart/form-data">
    <div class="dropdown">
                <?php if(empty($data->logo) || is_null($data->logo) ): ?>
                    <button data-id="siteData" class="postData btn btn-primary shadow-md flex items-center" aria-expanded="false">
                        Save 
                    </button>
                <?php else: ?>
                <button data-id="siteData" class="postData btn btn-warning shadow-md flex items-center" aria-expanded="false">
                        Update 
                    </button>
                <?php endif; ?>
        </div>

   <?php if(empty($data->website_title) || is_null($data->website_title)): ?>
            <input type="text"  name="website_title" class="intro-y form-control py-3 px-4 box pr-10 mt-5 placeholder-theme-13" value="" placeholder="Website Title" required>
            <?php else: ?>
            <input type="text"  name="website_title" class="intro-y form-control py-3 px-4 box pr-10 mt-5 placeholder-theme-13" value="<?php echo e($data->website_title); ?>" placeholder="Title" required>
            <?php endif; ?>
   <?php echo csrf_field(); ?>
    <div id="siteinfo" class="pos intro-y grid grid-cols-12 gap-5 mt-5">
   
        <!-- BEGIN: Post Content -->
        <div class="intro-y col-span-12 md:col-span-6 lg:col-span-6">
           
            <div class="post intro-y overflow-hidden box mt-5">
                <div class="post__content tab-content">
                    <div id="content" class="tab-pane p-5 active" role="tabpanel" aria-labelledby="content-tab">
                    <div class="mt-3 ">
                            <label class="form-label">Website Logo</label>
                            <div class="border-2 border-dashed dark:border-dark-5 rounded-md pt-4">
                                <div class="flex flex-wrap px-4">
                                    <div class="w-24 h-24 relative image-fit mb-5 mr-5 cursor-pointer zoom-in">
                                    <?php if(empty($data->logo) || is_null($data->logo) ): ?>
                                        <img id="logo_img" class="rounded-md get_img" alt="title image" src="" required>
                                        <input name="logo" type="hidden" id="logo">
                                    <?php else: ?>
                                        <img id="logo_img" class="rounded-md" alt="title image" src="<?php echo e($data->logo); ?>" required>
                                        <input name="logo" type="hidden" id="logo" value="<?php echo e($data->logo); ?>">
                                    <?php endif; ?>
                                        <div title="Remove this image?" class="remove tooltip w-5 h-5 flex items-center justify-center absolute rounded-full text-white bg-theme-6 right-0 top-0 -mr-2 -mt-2">
                                            <i data-feather="x" class="w-4 h-4"></i>
                                        </div>
                                    </div>
                                </div>
                                <div data-id="logo" class="px-4 pb-4 flex items-center cursor-pointer relative up_img_link">
                                    <i data-feather="image" class="w-4 h-4 mr-2"></i> <span class="text-theme-1 mr-1">Click to choose</span> Image from media
                                </div>
                            </div>
                        </div>
                        <div class="border border-gray-200 dark:border-dark-5 rounded-md p-5">
                            <div class="mt-5">
                                <div>
                                    <label for="post-form-7" class="form-label">Email</label>
                                    <?php if(empty($data->email) || is_null($data->email) ): ?>
                                    <input id="post-form-7" type="email" name="email" class="form-control" placeholder="Email" required>
                                    <?php else: ?>
                                    <input id="post-form-7" type="email" name="email" class="form-control" value="<?php echo e($data->email); ?>" placeholder="Email" required>
                                    <?php endif; ?>
                                </div>
                             
                            </div>
                        </div>  
                         <div class="border border-gray-200 dark:border-dark-5 rounded-md p-5">
                            <div class="mt-5">
                                <div>
                                <label for="post-form-7" class="form-label">Contact Number</label>
                                <?php if(empty($data->phone) || is_null($data->phone) ): ?>
                                    <input id="post-form-7" type="text" name="phone" class="form-control" placeholder="Contact number" required>
                                 <?php else: ?>
                                    <input id="post-form-7" type="text" name="phone" value="<?php echo e($data->phone); ?>" class="form-control" placeholder="Contact number" required>
                                 <?php endif; ?>   
                                </div>
                            </div>
                        </div>    
                        <div class="border border-gray-200 dark:border-dark-5 rounded-md p-5 ">
                            <div class="mt-5">
                                <div>
                                    <label for="post-form-7" class="form-label">Embed Google Map</label>
                                    <?php if(empty($data->gmap) || is_null($data->gmap) ): ?>
                                        <input id="post-form-7" type="text" name="gmap" class="form-control" placeholder="Gmap -> Search -> Share -> Embed -> Copy Html">
                                    <?php else: ?>
                                        <input id="post-form-7" type="text" name="gmap" value="<?php echo e($data->gmap); ?>" class="form-control" placeholder="Gmap -> Search -> Share -> Embed -> Copy Html">
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                         <div class="border border-gray-200 dark:border-dark-5 rounded-md p-5 ">
                            <div class="mt-5">
                                <div>
                                    <label for="post-form-7" class="form-label">Facebook link</label>
                                    <?php if(empty($data->facebook) || is_null($data->facebook) ): ?>
                                     <input id="post-form-7" type="text" name="facebook" class="form-control" placeholder="Facebook link">
                                    <?php else: ?>
                                    <input id="post-form-7" type="text" name="facebook" value="<?php echo e($data->facebook); ?>" class="form-control" placeholder="Facebook link">
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>  
                    </div>
                </div> 
            </div>
        </div>
        <!-- END: Post Content -->
            <!-- BEGIN: Post Content -->
            <div class="intro-y col-span-12 md:col-span-6 lg:col-span-6">
            <div class="post intro-y overflow-hidden box mt-5">
                <div class="post__content tab-content">
                    <div id="content" class="tab-pane p-5 active" role="tabpanel" aria-labelledby="content-tab">
                        <div class="mt-3 lg:col-span-8">
                            <label class="form-label">Site Favicon ( website icon )</label>
                            <div class="border-2 border-dashed dark:border-dark-5 rounded-md pt-4">
                                <div class="flex flex-wrap px-4">
                                    <div class="w-24 h-24 relative image-fit mb-5 mr-5 cursor-pointer zoom-in">
                                    <?php if(empty($data->favicon) || is_null($data->favicon) ): ?>
                                        <img id="favicon_img" class="rounded-md get_img " alt="title image" src="">
                                        <input name="favicon" type="hidden" id="favicon">
                                    <?php else: ?>
                                        <img id="favicon_img" class="rounded-md " alt="title image" src="<?php echo e($data->favicon); ?>">
                                        <input name="favicon" type="hidden" id="favicon" value="<?php echo e($data->favicon); ?>">
                                    <?php endif; ?>
                                        <div title="Remove this image?" class="remove tooltip w-5 h-5 flex items-center justify-center absolute rounded-full text-white bg-theme-6 right-0 top-0 -mr-2 -mt-2">
                                            <i data-feather="x" class="w-4 h-4"></i>
                                        </div>
                                    </div>
                                </div>
                                <div data-id="favicon" class="px-4 pb-4 flex items-center cursor-pointer relative up_img_link">
                                    <i data-feather="image" class="w-4 h-4 mr-2"></i> <span class="text-theme-1 mr-1">Click to choose</span> Image from media
                                </div>
                            </div>
                        </div>

                        <div class="border border-gray-200 dark:border-dark-5 rounded-md p-5">
                            <div class="mt-5">
                                <label for="post-form-7" class="form-label">Address</label>
                                <?php if(empty($data->add) || is_null($data->add) ): ?>
                                <textarea name="add" class="editor" placeholder="Add Address here" ></textarea>
                                <?php else: ?>
                                <div class="textDiv">
                                    <input name="add" class="form-control textareaPro" type="text" value="<?php echo e($data->add); ?>" >
                                    <textarea name="" class="editor" placeholder="Add Address here" value=""></textarea>
                                </div>
                                
                                <?php endif; ?>
                            </div>
                        </div>

                        <div class="border border-gray-200 dark:border-dark-5 rounded-md p-5 ">
                            <div class="mt-5">
                                <div>
                                    <label for="post-form-7" class="form-label">Instagram Link</label>
                                    <?php if(empty($data->instagram) || is_null($data->instagram) ): ?>
                                    <input id="post-form-7" type="text" name="instagram" class="form-control" placeholder="Instagram Link">
                                    <?php else: ?>
                                    <input id="post-form-7" type="text" name="instagram" class="form-control" value="<?php echo e($data->instagram); ?>" placeholder="Instagram Link">
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div> 
                         <div class="border border-gray-200 dark:border-dark-5 rounded-md p-5 ">
                            <div class="mt-5">
                                <div>
                                    <label for="post-form-7" class="form-label">Linkedin Link</label>
                                    <?php if(empty($data->linkedin) || is_null($data->linkedin) ): ?>
                                    <input id="post-form-7" type="text" name="linkedin" class="form-control" placeholder="Linkedin Link">
                                    <?php else: ?>
                                    <input id="post-form-7" type="text" name="linkedin" class="form-control" value="<?php echo e($data->linkedin); ?>" placeholder="Linkedin Link">
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>

                        <div class="border border-gray-200 dark:border-dark-5 rounded-md p-5 ">
                            <div class="mt-5">
                                <div>
                                    <label for="post-form-7" class="form-label">Twitter Link</label>
                                    <?php if(empty($data->twitter) || is_null($data->twitter) ): ?>
                                    <input id="post-form-7" type="text" name="twitter" class="form-control" placeholder="Twitter Link">
                                    <?php else: ?>
                                    <input id="post-form-7" type="text" value="<?php echo e($data->twitter); ?>" name="twitter" class="form-control" placeholder="Twitter Link">
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>

                    </div>
                </div> 
            </div>
        </div>
        <!------section 2--->
    </div>
    </form> 
<?php $__env->stopSection(); ?>
<?php echo $__env->make('../layout/' . $layout, \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\Laravel Projects\history_master\resources\views/pages/siteInfo.blade.php ENDPATH**/ ?>