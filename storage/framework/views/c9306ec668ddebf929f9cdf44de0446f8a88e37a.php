



<?php $__env->startSection('subcontent'); ?>
    <div class="grid grid-cols-12 gap-6">
        <div class="col-span-12">
            <div class="grid grid-cols-12 gap-6">

                <!-- BEGIN: Weekly Top Products -->
                <div class="col-span-12 text-center mt-6">
                    <div class="wlc text-center">
                       <h1>Welcome to <?php echo e($siteInfo->website_title); ?></h1>
                    </div>
                    <div class="inline-flex">
                        <img width="450" src="<?php echo e(asset($siteInfo->logo)); ?>">
                    </div>
                </div>
                <!-- END: Weekly Top Products -->
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('../layout/' . $layout, \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\Laravel Projects\history_master\resources\views/pages/dashboard.blade.php ENDPATH**/ ?>