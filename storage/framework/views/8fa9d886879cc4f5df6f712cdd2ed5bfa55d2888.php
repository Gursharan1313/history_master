

<?php $__env->startSection('body'); ?>
    <body class="main">
        <?php echo $__env->yieldContent('content'); ?>
        








            
<div class="sel_image">
    <div class="close" data-animate="fade"></div>
    <div class="ifrme p-5">
        <div class="grid grid-cols-12 gap-6">
        <div class="col-span-12 lg:col-span-3 xxl:col-span-2">
            <h2 class="intro-y text-lg font-medium mr-auto mt-2">File Manager</h2>
        </div>
        <div class="col-span-12 lg:col-span-12 xxl:col-span-12">
             <!-- BEGIN: Directory & Files -->
             <div class="intro-y grid grid-cols-12 gap-3 sm:gap-6 mt-5">
             <?php if(!empty($file)): ?>
                <?php $__currentLoopData = $file; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $file): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="intro-y col-span-4 sm:col-span-3 md:col-span-3 xxl:col-span-2">
                        <div class="file box rounded-md px-5 pt-8 pb-5 px-3 sm:px-5 relative zoom-in">
                            <?php if($file['location'] == 'images'): ?> 
                                <a href="<?php echo e(Storage::url($file['file_name'])); ?>" target="_blank" data-type="image" class="w-3/5 file__icon file__icon--image mx-auto">
                                    <div class="file__icon--image__preview image-fit">
                                        <img alt="image" src="<?php echo e(Storage::url($file['file_name'])); ?>">
                                    </div>
                                </a>
                            <?php elseif($file['location'] == 'videos'): ?>
                            <a href="<?php echo e(Storage::url($file['file_name'])); ?>" target="_blank"  data-type="video" class="w-3/5 mx-auto">
                                    <div class="file__icon--image__preview image-fit pb-2">
                                        <video width="100%" height="240" controls>
                                            <source src="<?php echo e(Storage::url($file['file_name'])); ?>" type="video/<?php echo e($file['file_type']); ?>">
                                            Your browser does not support the video tag.
                                        </video>
                                    </div>
                                </a>
                            <?php else: ?>
                                <a href="<?php echo e(Storage::url($file['file_name'])); ?>" target="_blank"  data-type="doc" class="w-3/5 file__icon file__icon--file mx-auto">
                                    <div class="file__icon__file-name"><?php echo e($file['file_type']); ?></div>
                                </a>
                            <?php endif; ?>
                            <a href="" class="block font-medium mt-4 text-center truncate"><?php echo e($file['file_name']); ?></a>
                            <div class="text-gray-600 text-xs text-center mt-0.5"><?php echo e($file['file_size']); ?></div>
                            <div class="absolute top-0 right-0 mr-2 mt-2 dropdown ml-auto">
                                <a class="dropdown-toggle w-5 h-5 block" href="javascript:;" aria-expanded="false">
                                    <i data-feather="more-vertical" class="w-5 h-5 text-gray-600"></i>
                                </a>
                                <div class="dropdown-menu w-40">
                                    <div class="dropdown-menu__content box dark:bg-dark-1 p-2">
                                        <a href="<?php echo e(Storage::url($file['file_name'])); ?>" target="_blank" class="flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2  rounded-md">
                                            <i data-feather="copy" class="w-4 h-4 mr-2"></i> Open in new tab
                                        </a>
                                        <a data-id="<?php echo e($file['id']); ?>" class="DelImg flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2  rounded-md">
                                            <i data-feather="trash" class="w-4 h-4 mr-2"></i> Delete
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php endif; ?>
            </div>
             <!-- BEGIN: Directory & Files -->
        </div>
    </div>
    </div>
</div>
        <!-- BEGIN: JS Assets-->
        <script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>
        <script src="<?php echo e(mix('dist/js/app.js')); ?>"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="<?php echo e(asset('dist/js/custom.js')); ?>"></script>
        <!-- END: JS Assets-->

        <?php echo $__env->yieldContent('script'); ?>
    </body>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('../layout/base', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\Laravel Projects\admin theme\dp-1\resources\views////layout/main.blade.php ENDPATH**/ ?>