

<?php $__env->startSection('subcontent'); ?>
<script src="<?php echo e(asset('dist/ckeditor/ckeditor.js')); ?>"></script>
	<script src="<?php echo e(asset('dist/ckeditor/sample.js')); ?>"></script>
    <div class="intro-y flex flex-col sm:flex-row items-center mt-8">
        <h2 class="text-lg font-medium mr-auto">Add New Page</h2>
        <?php if(empty($page_content) || is_null($page_content)): ?>
        <?php else: ?>
        <div class="w-full sm:w-auto flex mt-4 sm:mt-0">
           <!-- <button type="button" class="btn box text-gray-700 dark:text-gray-300 mr-2 flex items-center ml-auto sm:ml-0">
                <i class="w-4 h-4 mr-2" data-feather="eye"></i> Preview
            </button>-->
            <div class="dropdown">
                <button data-id="<?php echo e($page_content->id); ?>" class="delPage btn btn-danger shadow-md flex items-center" aria-expanded="false">
                    Delete 
                </button>
            </div>
        </div>
        <?php endif; ?>
    </div>
    <?php if(!empty($message)): ?>
        <?php if($message != "Fail"): ?>
        <div class="message">
            <div><?php echo e($message); ?></div>
        </div>
        <?php else: ?>
        <div class="fail">
            <div><?php echo e($message); ?></div>
        </div>
        <?php endif; ?>
    <?php endif; ?>
    <div class="intro-y flex flex-col sm:flex-row items-center mt-8">
        <?php $__currentLoopData = $pages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $page): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  
            <?php if($page->status == "on"): ?>
            <a href="/otherpage/<?php echo e($page->id); ?>"><button class="btn btn-success mr-2"><?php echo e($page->page_title); ?></button></a>
            <?php else: ?>
            <a href="/otherpage/<?php echo e($page->id); ?>"><button class="btn btn-danger mr-2"><?php echo e($page->page_title); ?></button></a>
            <?php endif; ?>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </div>
    <form id="othePageData" method="post" action="post-othePage-data">
   <?php echo csrf_field(); ?>
    <div id="post_blog" class="pos intro-y grid grid-cols-6 gap-5 mt-5">
        <!-- BEGIN: Post Content -->
        <div class="intro-y col-span-12 lg:col-span-8">
            <?php if(empty($page_content) || is_null($page_content)): ?>
            <?php else: ?>
            <input type="hidden"  name="page_id" value="<?php echo e($page_content->id); ?>" placeholder="Title">
            <?php endif; ?>
            <?php if(empty($page_content) || is_null($page_content)): ?>
            <input type="text"  name="page_title" class="intro-y form-control py-3 px-4 box pr-10 placeholder-theme-13" value="" placeholder="Title">
            <?php else: ?>
            <input type="text"  name="page_title" class="intro-y form-control py-3 px-4 box pr-10 placeholder-theme-13" value="<?php echo e($page_content->page_title); ?>" placeholder="Title">
            <?php endif; ?>
            <div class="post intro-y overflow-hidden box mt-5">
                <div class="post__tabs nav nav-tabs flex-col sm:flex-row bg-gray-300 dark:bg-dark-2 text-gray-600" role="tablist">
                    <a title="Fill in the article content" data-toggle="tab" data-target="#content" href="javascript:;" class="tooltip w-full sm:w-40 py-4 text-center flex justify-center items-center active" id="content-tab" role="tab" aria-controls="content" aria-selected="true">
                        <i data-feather="file-text" class="w-4 h-4 mr-2"></i> Content
                    </a>
                </div>
                <div class="post__content tab-content">
                    <div id="content" class="tab-pane p-5 active" role="tabpanel" aria-labelledby="content-tab">
                        <div class="border border-gray-200 dark:border-dark-5 rounded-md p-5">
                            <div class="font-medium flex items-center border-b border-gray-200 dark:border-dark-5 pb-5">
                                <i data-feather="chevron-down" class="w-4 h-4 mr-2"></i> Text Content
                            </div>
                            <div class="mt-5">
                            <?php if(empty($page_content) || is_null($page_content)): ?>
                                <textarea name="page_body" id="editor" placeholder="Content of the editor.">
                                </textarea>
                            <?php else: ?>
                            <textarea name="page_body" id="editor" placeholder="Content of the editor.">
                             <?php echo e($page_content->page_body); ?>

                                </textarea>
                            <?php endif; ?>
                            </div>
                        </div>
                        
                    </div>
                </div>
                <div class="intro-y box p-5">
                    <div class="form-check flex-col items-start mt-3">
                        <div class="font-medium flex items-center border-b border-gray-200 dark:border-dark-5 pb-5">
                            <i data-feather="chevron-down" class="w-4 h-4 mr-2"></i>Add Brand Carousel at Bottom
                        </div>
                        <?php if(empty($page_content) || is_null($page_content)): ?>
                        <input id="post-form-5" name="brand" class="form-check-switch" type="checkbox">
                        <?php else: ?>
                            <?php if(empty($page_content->brand) || is_null($page_content->brand) || $page_content->brand == "off"): ?>
                                <input id="post-form-5" name="brand" class="form-check-switch" type="checkbox">
                            <?php else: ?>
                                <input id="post-form-5" name="brand" class="form-check-switch" type="checkbox" checked>    
                            <?php endif; ?>
                        <?php endif; ?>
                    </div>
                </div>
                
                <div class="intro-y box p-5">
                    <div class="form-check flex-col items-start mt-3">
                        <div class="font-medium flex items-center border-b border-gray-200 dark:border-dark-5 pb-5">
                            <i data-feather="chevron-down" class="w-4 h-4 mr-2"></i>Add Testimonials at Bottom
                        </div>
                        <?php if(empty($page_content) || is_null($page_content)): ?>
                        <input id="post-form-5" name="testimonial" class="form-check-switch" type="checkbox">
                        <?php else: ?>
                            <?php if(empty($page_content->testimonial) || is_null($page_content->testimonial) || $page_content->testimonial == "off"): ?>
                            <input id="post-form-5" name="testimonial" class="form-check-switch" type="checkbox" >
                            <?php else: ?>
                            <input id="post-form-5" name="testimonial" class="form-check-switch" type="checkbox" checked>
                            <?php endif; ?>
                        <?php endif; ?>
                    </div>
                </div>
                
                <div class="intro-y box p-5">
                    <div class="form-check flex-col items-start mt-3">
                        <div class="font-medium flex items-center border-b border-gray-200 dark:border-dark-5 pb-5">
                            <i data-feather="chevron-down" class="w-4 h-4 mr-2"></i> Publish
                        </div>
                        <?php if(empty($page_content) || is_null($page_content)): ?>
                        <input id="post-form-5" name="status" class="form-check-switch" type="checkbox">
                        <?php else: ?>
                            <?php if(empty($page_content->status) || is_null($page_content->status) || $page_content->status == "off"): ?>
                            <input id="post-form-5" name="status" class="form-check-switch" type="checkbox">
                            <?php else: ?>
                            <input id="post-form-5" name="status" class="form-check-switch" type="checkbox" checked>
                            <?php endif; ?>
                        <?php endif; ?>
                    </div>
                </div>

                <div class="w-full sm:w-auto flex mt-4 sm:mt-0 p-5">
                    <div class="dropdown">
                    <?php if(empty($page_content) || is_null($page_content)): ?>
                        <button class="btn btn-primary shadow-md flex items-center" aria-expanded="false">
                            Save 
                        </button>
                    <?php else: ?>
                    <button class="btn btn-warning shadow-md flex items-center" aria-expanded="false">
                           Update
                        </button>
                    <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
        <!-- END: Post Content -->
    </div>
    </form> 

<script>
	initSample();
</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('../layout/' . $layout, \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\Laravel Projects\admin theme\dp-1\resources\views/pages/otherPage.blade.php ENDPATH**/ ?>