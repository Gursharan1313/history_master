@extends('layout.layout')
    @section('content')

        <div id="content" class="site-content">
            <div class="page-header flex-middle">
                <div class="container">
                    <div class="inner flex-middle">
                        <h1 class="page-title">Contact</h1>
                        <ul id="breadcrumbs" class="breadcrumbs none-style">
                            <li><a href="index.html">Home</a></li>
                            <li class="active">Contact</li>
                        </ul>    
                    </div>
                </div>
            </div>
            <section class="contact-page">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="contact-left">
                                <div class="ot-heading">
                                    <span>// contact details</span>
                                    <h2 class="main-heading">Contact us</h2>
                                </div>
                                <div class="space-5"></div>
                                <p>Give us a call or drop by anytime, we endeavour to answer all enquiries within 24 hours on business days. We will be happy to answer your questions.</p>
                                <div class="contact-info box-style1">
                                    <i class="flaticon-world-globe"></i>                    
                                    <div class="info-text">
                                        <h6>Our Address:</h6>
                                        <p>Digital Personas Pvt Ltd</p>
                                        <p><a href="https://goo.gl/maps/2NsuaWW5CF3VC2GF6" target="_blank">Spaces & More Business Park. Level-3 C2, Vittal Rao Nagar Madhapur, HITECH City Hyderabad 500081.</a></p>
                                    </div>
                                </div>
                                <div class="contact-info box-style1">
                                    <i class="flaticon-envelope"></i>
                                    <div class="info-text">
                                        <h6>Our Mailbox:</h6>
                                        <p><a href="mailto:info@digitalpersonas.com" target="_blank">info@digitalpersonas.com</a></p>
                                    </div>
                                </div>
                                <div class="contact-info box-style1">
                                    <i class="flaticon-phone-1"></i>
                                    <div class="info-text">
                                        <h6>Our Phone:</h6>
                                        <p><a href="tel:040-4018 1212">040-4018 1212</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <form action="contact.php" method="post" class="wpcf7">
                                <div class="main-form">
                                    <h2>Ready to Get Started?</h2>
                                    <p class="font14">Your email address will not be published. Required fields are marked *</p>
                                    <p>
                                        <input type="text" name="name" value="" size="40" class="" aria-required="true" aria-invalid="false" placeholder="Your Name *" required>
                                    </p>
                                    <p>
                                        <input type="email" name="email" value="" size="40" class="" aria-required="true" aria-invalid="false" placeholder="Your Email *" required>
                                    </p>
                                    <p>
                                        <textarea name="message" cols="40" rows="10" class="" aria-invalid="false" placeholder="Message..." required></textarea>
                                    </p>
                                    <p><button type="submit" class="octf-btn octf-btn-light">Send Message</button>
                                    </p>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
            <div class="no-padding">
                <div class="map">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3806.391490405193!2d78.38352041452967!3d17.440966555849055!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xb36d9691ef299f9d!2sSpaces+%26+More+Business+Park!5e0!3m2!1sen!2sin!4v1563444299572!5m2!1sen!2sin" height="500" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                </div>
            </div>
        </div>

    @endsection