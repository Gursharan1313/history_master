@extends('layout.layout')
    @section('content')
        <div id="content" class="site-content">
            <div class="page-header flex-middle">
                <div class="container">
                    <div class="inner flex-middle">
                        <h1 class="page-title">Sap Fiori</h1>
                        <ul id="breadcrumbs" class="breadcrumbs none-style">
                            <li><a href="index.php">Home</a></li>
                            <li>Services</li>
                            <li class="active">Sap Fiori</li>
                        </ul>    
                    </div>
                </div>
            </div>
            <section id="what_is" class="service-page  inner-pages">
                <div class="container">
                    <div class="space-55"></div>
                    <div class="row">
                        <div class="col-md-6 ">
                        <div class="space-20"></div>
                                    <h2>What is Fiori?</h2>
                                  
                            <div class="ot-accordions">
                                <div class="acc-item">
                                    <span class="acc-toggle">
                                    <img src="images/icons/005-user.png" width="50px">
                                            ROLE-BASED 
                                        <i class="down flaticon-download-arrow"></i><i class="up flaticon-up-arrow"></i>
                                    </span>
                                    <div class="acc-content">
                                        <p>SAP Fiori is designed for your business, your needs, and how you work. It draws from our broad insights on the multifaceted roles of today’s workforce. SAP Fiori provides the right information at the right time and reflects the way you actually work.</p>              
                                    </div>
                                </div>
                                <div class="acc-item">
                                    <span class="acc-toggle">
                                    <img src="images/icons/001-quality-of-life.png" width="50px">
                                            DELIGHTFUL
                                        <i class="down flaticon-download-arrow"></i><i class="up flaticon-up-arrow"></i>
                                    </span>
                                    <div class="acc-content">
                                        <p>Apart from making you work smarter, SAP Fiori also enriches your work experience by allowing you to simply do your job.. </p>            
                                    </div>
                                </div>
                                <div class="acc-item">
                                    <span class="acc-toggle">
                                    <img src="images/icons/002-computer.png" width="50px">
                                            COHERENT 
                                        <i class="down flaticon-download-arrow"></i><i class="up flaticon-up-arrow"></i>
                                    </span>
                                    <div class="acc-content">
                                        <p>Whether you fulfill a sales order, review your latest KPIs, or manage leave requests – SAP Fiori adheres to a consistent interaction and visual design language. Across the enterprise, you enjoy the same intuitive and consistent experience.</p>
                                    </div>
                                </div>
                                <div class="acc-item">
                                    <span class="acc-toggle">
                                    <img src="images/icons/004-easy-to-use.png" width="50px">
                                        SIMPLE
                                        <i class="down flaticon-download-arrow"></i><i class="up flaticon-up-arrow"></i>
                                    </span>
                                    <div class="acc-content">
                                        <p>With SAP Fiori, you can complete your job intuitively and quickly. SAP Fiori helps you focus on what is important – essential functions are easy to use and you can personalize the experience to focus on your relevant tasks and activities.</p>
                                    </div>
                                </div>
                                <div class="acc-item">
                                    <span class="acc-toggle">
                                    <img src="images/icons/003-responsive-design.png" width="50px">
                                            ADAPTIVE
                                        <i class="down flaticon-download-arrow"></i><i class="up flaticon-up-arrow"></i>
                                    </span>
                                    <div class="acc-content">
                                        <p>SAP Fiori enables you to work how and where you want, regardless of the device you use. And, it provides relevant information that allows for instant insight.</p>
                                    </div>
                                </div>
                            </div>
                      
                            </div>
                    
                                <div class="col-lg-6 col-md-6 ">
                                    <div id="sp_image" class="content-box">

                                    </div>
                                </div>
                     
                        </div>
                         <div class="row" style="margin-top: 5em;">
                            <div class="col-lg-6 col-md-6">
                                <img src="images/sapfiori.png">
                            </div>
                            <div class="col-lg-6 col-md-6 design_box">
                                <h4 class="main-heading">Design with SAP Fiori</h4>
                                <p>Use the Ul resources of our award-winning user experience for designing great enterprise software. We are constantly evolving it to make sure your users keep getting the best enterprise UX in the industry.</p>
                                <a href="contact.php" ><button class="octf-btn octf-btn-primary">Design with SAP Fiori</button></a>
                            </div>
                        </div>
                </div>
            </section>
              <section id="our_offers" class="our_offering">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 mb-12 mb-sm-12">
                            <div class="ot-heading mb-0">
                                <h2 class="main-heading">Our offering</h2>
                                <span>// Our delivery experience and teamwork with SAP Fort 4 Uts product teams make sure that your solution is cutting edge with</span>
                            </div>
                        </div>
                    </div>
                    <div class="space-55"></div>
                    <div class="row">
                        <div class="col-lg-4 col-md-4 mb-30 pd-5">
                           <div class="service-box s-box aos-item" data-aos="zoom-out-up">
                                <span class="big-number">01</span>
                                <div class="icon-main color-s2"><span class="flaticon-correct"></span></div>
                                <div class="content-box">
                                    <h5>Fiori Standard App Deployment</h5>
                                </div>
                            </div>
                            <div class="service-box s-box aos-item second" data-aos="zoom-out-up">
                                <span class="big-number">02</span>
                                <div class="icon-main color-s2"><span class="flaticon-correct"></span></div>
                                <div class="content-box">
                                    <h5>Fiori Extensions</h5>
                                </div>
                            </div>
                            <div class="service-box s-box aos-item" data-aos="zoom-out-up">
                                <span class="big-number">03</span>
                                <div class="icon-main color-s2"><span class="flaticon-correct"></span></div>
                                <div class="content-box">
                                    <h5>Fiori Custom App Development</h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 mb-30 pd-5">
                           <div class="center_img">
                               <img src="images/%5Bremoval.ai%5D_tmp-6036b3b7c5ee4.png">
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 mb-30 pd-5">
                           <div class="service-box s-box aos-item" data-aos="zoom-out-up">
                                <span class="big-number">04</span>
                                <div class="icon-main color-s2"><span class="flaticon-correct"></span></div>
                                <div class="content-box">
                                    <h5>Fiori Elements</h5>
                                </div>
                            </div>
                            <div class="service-box s-box aos-item fifth" data-aos="zoom-out-up">
                                <span class="big-number">05</span>
                                <div class="icon-main color-s2"><span class="flaticon-correct"></span></div>
                                <div class="content-box">
                                    <h5>Fiori HANA Based Apps Development</h5>
                                </div>
                            </div>
                            <div class="service-box s-box aos-item" data-aos="zoom-out-up">
                                <span class="big-number">06</span>
                                <div class="icon-main color-s2"><span class="flaticon-correct"></span></div>
                                <div class="content-box">
                                    <h5>Fiori Operations Support</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                  </div>
            </section> 
   
            <section class="timeline2">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 mb-12 mb-sm-12">
                            <div class="text-center ot-heading mb-0">
                                <h2 class="main-heading">Unvired SAP Fiori App Development Services</h2>
                                <span>// End-to-end SAP Fiori Application Development services to meet your custom requirements. Improve run-time performance and expand the functionalities with SAP Fiori applications.</span>
                            </div>
                        </div>
                    </div>
                    <div class="space-55"></div>
                    <div class="row">
                        <div class="col-md-6 offset-md-3">
                            <div class="timeline exp">
                                    <div class="tl-block wow fadeInUp" data-wow-delay="0">
                                        <div class="tl-time">
                                            <h4>Design</h4>
                                        </div>
                                        <div class="tl-bar">
                                            <div class="tl-line"></div>
                                        </div>
                                        <div class="tl-message">
                                            <div class="tl-icon">&nbsp;</div>
                                            <div class="tl-main">
                                                <h4 class="id-color">User Interface/ Experience Design</h4>
                                                Simplify usability for a seamless interaction with SAP system Achieve consistent native like experience across platfo with fluid intuitive design
                                            </div>
                                        </div>
                                    </div>
    
                                    <div class="tl-block wow fadeInUp" data-wow-delay=".3s">
                                        <div class="tl-time">
                                            <h4>Development</h4>
                                        </div>
                                        <div class="tl-bar">
                                            <div class="tl-line"></div>
                                        </div>
                                        <div class="tl-message">
                                            <div class="tl-icon">&nbsp;</div>
                                            <div class="tl-main">
                                                <h4 class="id-color">Custom App Development</h4>
                                                Build highly flexible and scalable Fiori/UI5 apps in a short amount of time Customize any element of existing Fiori apps while adhering to SAP Fiori Design Guidelines
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tl-block wow fadeInUp" data-wow-delay=".3s">
                                        <div class="tl-time">
                                            <h4>Delivery</h4>
                                        </div>
                                        <div class="tl-bar">
                                            <div class="tl-line"></div>
                                        </div>
                                        <div class="tl-message">
                                            <div class="tl-icon">&nbsp;</div>
                                            <div class="tl-main">
                                                <h4 class="id-color">Delivery</h4>
                                                Quickly deploy and run custom Fiori applications Optimize resource usage and get complete guided support for any type of deployment on premise cloud or hybrid.
                                            </div>
                                        </div>
                                    </div>
    
                                    <div class="tl-block wow fadeInUp" data-wow-delay=".6s">
                                        <div class="tl-time">
                                            <h4>END USER</h4>
                                        </div>
                                        <div class="tl-bar">
                                            <div class="tl-line"></div>
                                        </div>
                                        <div class="tl-message">
                                            <div class="tl-icon">&nbsp;</div>
                                            <div class="tl-main">
                                                <h4 class="id-color">Consultation
                                                </h4>
                                                Get detailed information on user licences, version upgrades to S/4HANA and custom screen personas to implement Fiori apps Maximize ROI as per requirements
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </section>

            <section id="built_in">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 ot-heading">
                            <h2>Everything you need, built in</h2>
                            <span>//Provide you business with constant innovation.</span>
                            <div class="space-20"></div>
                        </div>
                    </div>
                <div class="ot-industries">
                    <div class="row">
                        <div class="industries-inner col-md-4 col-lg-4 aos-item"  data-aos="fade-right" data-aos-offset="200" data-aos-easing="ease-in-sine">
                            <div class="indus-item ">
                                <div class="item-inner">
                                    <div class="overlay"></div>
                                    <div class="i-image">
                                        <img src="images/sap_d.png" width="120px" alt="">
                                    </div>
                                    <div class="iinfo">
                                        <h6>SAPUI5</h6>
                                        <div class="itext">
                                        The development toolkit provides you with everything you need to create stunning coherent and deeply integrated apps.
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                            <div class="indus-item col-md-4 col-lg-4 aos-item"  data-aos="fade-right" data-aos-offset="300"  data-aos-easing="ease-in-sine">
                                <div class="item-inner">
                                    <div class="overlay"></div>
                                    <div class="i-image">
                                        <img src="images/sap_d2.png" width="120px" alt="">
                                    </div>
                                    <div class="iinfo">
                                        <h6>SAP Fiori elements</h6>
                                        <div class="itext">
                                        This is where the front end magic happens. SAP Fiori elements handle the UI code for you, so you can focus on business logic and back-end services.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <div class="indus-item col-md-4 col-lg-4 aos-item"  data-aos="fade-right" data-aos-offset="350"  data-aos-easing="ease-in-sine">
                                <div class="item-inner">
                                    <div class="overlay"></div>
                                    <div class="i-image">
                                        <img src="images/sap_d3.png" width="120px" alt="">
                                    </div>
                                    <div class="iinfo">
                                        <h6>UI5 Web Components</h6>
                                        <div class="itext">
                                        Angular, React, Vue, UI5 - being any UI framework you want to the table. That's what we call an open technology agnostic environment.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    <section id="fiori_ux">
        <div class="row">
            <div class="col-md-12 ot-heading text-center">
                <h2>Key UI tools and technology from SAP to deliver SAP Fiori UX</h2>
                <span>//Simplify Overview</span>
                <div class="space-20"></div>
            </div>
            <div class="col-md-12 ot-heading text-center">
                <div>
                    <img src="images/fiori_key.png" alt="Simplify Overview">
                </div>
            </div>
        </div>
        </section>
@endsection