@extends('layout.layout')
    @section('content')

        <div id="content" class="site-content">
            <div class="page-header flex-middle">
                <div class="container">
                    <div class="inner flex-middle">
                        <h1 class="page-title">Staff Augmentation</h1>
                        <ul id="breadcrumbs" class="breadcrumbs none-style">
                            <li><a href="/">Home</a></li>
                            <li>Services</li>
                            <li class="active">Staff Augmentation</li>
                        </ul>    
                    </div>
                </div>
            </div>
            <section class="it-staff-page  inner-pages">
                <div class="container">
                     <div class="space-55"></div>
                    <div class="row">
                        <div class="col-md-6 col-sm-8 mb-4 mb-sm-0">
                            <div class="ot-heading bb mb-0">
                                <span>// Flexible IT staffing to round out your team or project.</span>
                                <h2 class="main-heading">IT STAFF AUGMENTATION</h2>
                            </div>
                        </div>
                    </div>
                   <div class="space-55"></div>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="content-box">
                                <p>IT Staff Augmentation Services from us can provide critical skill sets for your business or IT objectives, filling gaps in your current team and providing staffing solutions that will help ensure your technology initiatives keep pace with your business needs.</p>
                                <p><b>With IT Staff Augmentation Services from Digital Persona, you can:</b></p>
                                <ul>
                                    <li>Access certified IT professionals including Project Managers, Architects, Systems/Business Analysts, Software Developers, Infrastructure Specialists, and more</li>
                                    <li>Gain critical skill sets for your IT operations</li>
                                    <li>Benefit from project-specific delivery expertise</li>
                                    <li>Support your IT operations</li>
                                </ul>
                            </div>
                        </div> 
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="content-box">
                               <form action="contact.php" method="post" class="wpcf7">
                                <div class="main-form">
                                    <h3>Speak to an IT Experted?</h3>
                                    <p>
                                        <input type="text" name="name" value="" size="40" class="" aria-required="true" aria-invalid="false" placeholder="Your Name *" required="">
                                    </p>
                                    <p>
                                        <input type="email" name="email" value="" size="40" class="" aria-required="true" aria-invalid="false" placeholder="Your Email *" required="">
                                    </p>
                                    <p>
                                        <textarea name="message" cols="40" rows="10" class="" aria-invalid="false" placeholder="Message..." required=""></textarea>
                                    </p>
                                    <p><button type="submit" class="octf-btn octf-btn-light">Send Message</button>
                                    </p>
                                </div>
                            </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
             <section id="it_staff" class="our_offering">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 mb-12 mb-sm-12">
                            <div class="ot-heading bb mb-0">
                                <h2 class="main-heading">Our IT Staff Augmentation Solutions</h2>
                            </div>
                        </div>
                    </div>
                    <div class="space-55"></div>
                    <div class="row">
                          <div class="ot-tabs">
                                <ul class="tabs-heading unstyle">
                                    <li class="tab-link octf-btn current" data-tab="tab-1">Project Delivery</li>
                                    <li class="tab-link octf-btn" data-tab="tab-2">Business Operations</li>
                                </ul>
                                <div id="tab-1" class="tab-content current">
                                    <h3>Project Delivery</h3>
                                    <p>Getting your project completed on time takes a varied talent base and you can draw from our pool of expert team members to help you see it through to completion. Some of our experts include:</p>
                                    <p><b>Project Managers:</b> Carrying extensive certification and industry knowledge, our project managers are ready to manage and implement a range of project types.</p>
                                    <p><b>Architects:</b> How does your IT environment serve your business? Our system architects will work with your company's leadership and IT staff to determine the optimal configuration that will support your project goals.
                                    <p><b>Systems/Business Analysts:</b> Our analysts are available to add value to your operations by identifying actual requirements and recommending solutions, allowing you to proactively stay ahead of the curve when it comes to your technology operations.</p>
                                    <p><b>Software Developers:</b> From web and mobile application creation through integration and strategy support, our software developers are ready to handle all of your application development needs.</p>
                                    <p><b>Infrastructure Specialists:</b> When you need outstanding management and support for your IT infrastructure, our specialists are available to ensure that your systems work efficiently and deliver value to your organization.</p>
                                </div>
                                <div id="tab-2" class="tab-content">
                                    <h3>Business Operations</h3>
                                    <p>Whether you need operational level IT management or support for an existing team, our business operations staff augmentation services are ready to assist. Some of the ways we can help to maximize your business operations include:
                                    <p><b>Business Process Optimization:</b> Our analysts are ready to determine your overall process efficiency and effectiveness and can provide guidance on the best way for your company to proceed with best practices.</p>
                                    <p><b>Package Selection Consulting:</b> When it comes to package selection, our experts have the tools and knowledge to help you choose the right setup for your company's needs. We can provide assistance at every step of the process, from collecting technical requirements through the RFP stage and post-vendor management.</p>
                                    <p>Buy vs Build: When you're trying to make the decision whether to buy or build an IT asset such as an application or your technological infrastructure, we can provide assistance in determining your best course of action.</p>
                                </div>
                            </div>
                    </div>
                </div>
            </section>


       
        <section id="it_staff_highlight" class="highlight-box bg-dark-primary">
            <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-sm-12 mb-12 mb-sm-12">
                            <img src="images/Staff-Augmentation-min.png">
                        </div>
                        <div class="col-md-6 col-sm-12 mb-12 mb-sm-12">
                            <div class="bb ot-heading ">
                                <h2 class="main-heading">Our IT Staff Augmentation Services</h2>
                            </div>
                            <p><b>Experiencing a Skills Gap in Your Team or Project?</b></p>
                            <p>With a range of experts to choose from, We can help to augment your IT staff as needed. Whatever type of assistance you require, we’ve got the staffing augmentation solution to get you where you need to go.When you’re short of a necessary skill to get your project delivered, trust the technology experts to help fill the gap.</p>
                        </div>
                    </div>

            </div>
        </section>
            
        <section id="">
            <div class="container">
                    <div class="row">
                       
                        <div class="col-md-6 col-sm-12 mb-12 mb-sm-12">
                            <div class="bb ot-heading ">
                                <h2 class="main-heading">Trust the IT Staffing Augmentation Leaders</h2>
                            </div>
                            <p>Our deep pool of business and technology consultants are ready to provide assistance when you need it, ensuring you always have an on-call selection of experts as your situation dictates. Our IT staff augmentation services are able to lend unparalleled knowledge and guidance to your resource base, giving you the flexibility to adapt to an ever-changing competitive landscape.</p>
                            <p><b>Request a Free Consultation:</b>For a consultation to learn how we can help you gain the agility to adapt to an evolving landscape, <a href="contact.php">contact us.</a></p>
                        </div>
                        
                         <div class="col-md-6 col-sm-12 mb-12 mb-sm-12">
                            <img src="images/office_work_05.jpg">
                        </div>
                    </div>

            </div>
        </section>
            
        </div>


    @endsection