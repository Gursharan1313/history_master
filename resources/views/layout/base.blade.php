<!DOCTYPE html>
<html>
<!-- BEGIN: Head -->
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
   
    @if(!empty($pageName))
        <title>{{$pageName." - $siteInfo->website_title"}}</title>
     @else
          <title> {{"Dashboard - $siteInfo->website_title"}}</title>    
    @endif
    @if(!empty($siteInfo->favicon))
        <link href="{{asset($siteInfo['favicon'])}}" rel="shortcut icon">
    @elseif(!empty($siteInfo->logo))
        <link href="{{asset($siteInfo['logo'])}}" rel="shortcut icon">
    @endif
    <!-- BEGIN: CSS Assets-->
    <link rel="stylesheet" href="{{ mix('dist/css/app.css') }}" />
    <link rel="stylesheet" href="{{ asset('dist/css/app1.css') }}" />
    <link rel="stylesheet" href="{{ asset('dist/css/app2.css') }}" />
    <link rel="stylesheet" href="{{ asset('dist/css/app3.css') }}" />
    <link rel="stylesheet" href="{{ asset('dist/css/app4.css') }}" />
    <link rel="stylesheet" href="{{ asset('dist/css/custom.css') }}" />
 
    <!-- END: CSS Assets-->
</head>
<!-- END: Head -->

@yield('body')

</html>