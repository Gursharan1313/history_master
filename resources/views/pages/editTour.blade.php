@extends('../layout/' . $layout)

@section('subcontent')
@if ($errors->any())
    <div class="alert alert-danger" style="display: block;">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
    <div class="intro-y flex flex-col sm:flex-row items-center mt-8">
        <h2 class="text-lg font-medium mr-auto">Add New Tour</h2>
        <div class="w-full sm:w-auto flex mt-4 sm:mt-0">
           <!-- <button type="button" class="btn box text-gray-700 dark:text-gray-300 mr-2 flex items-center ml-auto sm:ml-0">
                <i class="w-4 h-4 mr-2" data-feather="eye"></i> Preview
            </button>-->
        </div>
        @if (Session::has('msg'))
            <div class="alert alert-success alert-dismissible show flex items-center mb-5" role="alert"> <i data-feather="alert-triangle" class="w-6 h-6 mr-2"></i> {{Session::get('msg')}} <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"> <i data-feather="x" class="w-4 h-4"></i> </button> </div>
        @endif
        @if (Session::has('error'))
        <div class="alert alert-danger alert-dismissible show flex items-center mb-2" role="alert"> <i data-feather="alert-octagon" class="w-6 h-6 mr-2"></i>  {{Session::get('error')}} <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"> <i data-feather="x" class="w-4 h-4"></i> </button> </div>
        @endif
    </div>
   <form id="postData"  method="post" action="post-tour-data" class="tourPost">
            <div class="dropdown">
                <button data-id="postData" class="postData btn btn-warning shadow-md flex items-center" aria-expanded="false">
                    update 
                </button>
            </div>

   @csrf
    <div id="post_blog" class="pos intro-y grid grid-cols-12 gap-5 mt-5">
        <!-- BEGIN: Post Content -->
        <div class="intro-y col-span-12 lg:col-span-8">
            <input type="text"  name="tour_name" class="intro-y form-control py-3 px-4 box pr-10 placeholder-theme-13" placeholder="Title" value="{{$tour->tour_name}}"  required>
            <div class="post intro-y overflow-hidden box mt-5">
                <div class="post__tabs nav nav-tabs flex-col sm:flex-row bg-gray-300 dark:bg-dark-2 text-gray-600" role="tablist">
                    <a title="Fill in the article content" data-toggle="tab" data-target="#content" href="javascript:;" class="tooltip w-full sm:w-40 py-4 text-center flex justify-center items-center active" id="content-tab" role="tab" aria-controls="content" aria-selected="true">
                        <i data-feather="file-text" class="w-4 h-4 mr-2"></i> Content
                    </a>
                </div>
                <div class="post__content tab-content">
                    <div id="content" class="tab-pane p-5 active" role="tabpanel" aria-labelledby="content-tab">
                        <div class="border border-gray-200 dark:border-dark-5 rounded-md p-5">
                            <div class="font-medium subHeading flex items-center border-b border-gray-200 dark:border-dark-5 pb-5">
                                Tour Overview <i data-feather="chevron-down" class="w-4 h-4 mr-2"></i> 
                            </div><hr>
                            <div class="mt-5">
                                <textarea name="overview" class="editor" placeholder="Content of the editor." required>
                                </textarea>
                            </div>
                        </div>

                    </div>
                </div>
  
                <div class="post__content tab-content">
                    <div id="content" class="tab-pane p-5 active" role="tabpanel" aria-labelledby="content-tab">
                        <div class="border border-gray-200 dark:border-dark-5 rounded-md p-5">
                            <div class="font-medium subHeading flex items-center border-b border-gray-200 dark:border-dark-5 pb-5">
                                 Itinerary <i data-feather="chevron-down" class="w-4 h-4 mr-2"></i>
                            </div><hr>

                            <div id="itinerary">
                          
                                    <div>
                                        <label for="post-form-7" id="day1" class="day form-label mt-10 mb-5 ">Day 1</label>
                                    </div> 
                                    <div>
                                        <label for="post-form-7" class="form-label">Itinerary Heading</label>
                                        <input id="post-form-7" type="text" name="iheading[]" class="iHead form-control" placeholder="Itinerary Heading" required>
                                    </div>
                                    <div class="mt-3">
                                        <label class="form-label">Image</label>
                                            <div class="border-2 border-dashed dark:border-dark-5 rounded-md">
                                                <div class="flex flex-wrap px-4">
                                                    <div class="w-24 h-24 relative image-fit mb-5 mr-5 cursor-pointer zoom-in">
                                                        <img id="it_1_img" class="rounded-md get_img" alt="title image" src="">
                                                        <div id="vid_show">
                                                        </div>
                                                        <input name="iImg[]" class="iImage" type="hidden" id="it_1">
                                                        <div title="Remove this image?" class="remove tooltip w-5 h-5 flex items-center justify-center absolute rounded-full text-white bg-theme-6 right-0 top-0 -mr-2 -mt-2">
                                                            <i data-feather="x" class="w-4 h-4"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-id="it_1" class="px-4  flex items-center cursor-pointer relative up_img_link">
                                                    <i data-feather="image" class="w-4 h-4 mr-2"></i> <span class="text-theme-1 mr-1">Click to choose</span> Image from media
                                                </div>
                                            </div>
                                        </div>
                                    <div class="mt-5">
                                        <label class="form-label">Itinerary Content</label>
                                        <textarea name="iContent[]"class="form-control" placeholder="Content of the editor." required>
                                        </textarea>
                                    </div>

                            </div>

                        </div>
                    </div>
                </div>
                <div class="pt-0 p-5 pl-10">
                    <button id="addNewIt" data-id="1" class="btn btn-warning">Add Other Day</button>
                </div>

            </div>

        </div>
        <!-- END: Post Content -->
        <!-- BEGIN: Post Info -->
        <div class="col-span-12 lg:col-span-4">
            <div class="intro-y box p-5">
                        <div class="mt-5">
                                <div>
                                    <label for="post-form-10" class="form-label">Tour Location</label>
                                    <input id="post-form-10" type="text" name="location" class="form-control" placeholder="Tour Location" required>
                                </div>
                                <div class="mt-3">
                                    <label class="form-label">Main Background Image</label>
                                    <div class="border-2 border-dashed dark:border-dark-5 rounded-md pt-4">
                                        <div class="flex flex-wrap px-4">
                                            <div class="w-24 h-24 relative image-fit mb-5 mr-5 cursor-pointer zoom-in">
                                                <img id="mainbg_img" class="rounded-md get_img" alt="title image" src="">
                                                <div id="vid_show">
                                                </div>
                                                <input name="bgImg" type="hidden" id="mainbg">
                                                <div title="Remove this image?" class="remove tooltip w-5 h-5 flex items-center justify-center absolute rounded-full text-white bg-theme-6 right-0 top-0 -mr-2 -mt-2">
                                                    <i data-feather="x" class="w-4 h-4"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div data-id="mainbg" class="px-4 pb-4 flex items-center cursor-pointer relative up_img_link">
                                            <i data-feather="image" class="w-4 h-4 mr-2"></i> <span class="text-theme-1 mr-1">Click to choose</span> Image from media
                                        </div>
                                    </div>
                                </div>
                            </div>
                <div class="mt-3">
                    <label for="post-form-3" class="form-label">Categories</label>
                    <select data-placeholder="Select categories" name="categories[]" class="tail-select w-full" id="post-form-3" multiple>
                    @foreach($cat as $cat)
                        <option value="{{$cat->id}}">{{$cat->name}}</option>
                    @endforeach
                    </select>
                </div>
                <div class="mt-3">
                    <label for="post-form-4" class="form-label">Tags</label>
                    <select data-placeholder="Select Tags" name="tags[]" class="tail-select w-full" id="post-form-4" multiple>
                    @foreach($tag as $tag)
                        <option value="{{$tag->id}}">{{$tag->name}}</option>
                    @endforeach
                    </select>
                </div>
                <div class="form-check flex-col items-start mt-3">
                    <label for="post-form-5" class="form-check-label ml-0 mb-2">Published</label>
                    <input id="post-form-5" name="status" class="form-check-switch" type="checkbox">
                </div>
            </div>
        </div>
        <!-- END: Post Info -->
    </div>
    </form> 
    
@endsection