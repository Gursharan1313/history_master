@extends('../layout/' . $layout)

@section('subcontent')
    <div class="intro-y flex flex-col sm:flex-row items-center mt-8">
        <h2 class="text-lg font-medium mr-auto">Site Info</h2>
        <div class="w-full sm:w-auto flex mt-4 sm:mt-0">
           <!-- <button type="button" class="btn box text-gray-700 dark:text-gray-300 mr-2 flex items-center ml-auto sm:ml-0">
                <i class="w-4 h-4 mr-2" data-feather="eye"></i> Preview
            </button>-->
        </div>
        @if (Session::has('msg'))
            <div class="alert alert-success alert-dismissible show flex items-center mb-5" role="alert"> <i data-feather="alert-triangle" class="w-6 h-6 mr-2"></i> {{Session::get('msg')}} <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"> <i data-feather="x" class="w-4 h-4"></i> </button> </div>
        @endif
        @if (Session::has('error'))
        <div class="alert alert-danger alert-dismissible show flex items-center mb-2" role="alert"> <i data-feather="alert-octagon" class="w-6 h-6 mr-2"></i>  {{Session::get('error')}} <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"> <i data-feather="x" class="w-4 h-4"></i> </button> </div>
        @endif
    </div>
       
   <form id="siteData" method="post" action="siteInfoData" enctype="multipart/form-data">
    <div class="dropdown">
                @if(empty($data->logo) || is_null($data->logo) )
                    <button data-id="siteData" class="postData btn btn-primary shadow-md flex items-center" aria-expanded="false">
                        Save 
                    </button>
                @else
                <button data-id="siteData" class="postData btn btn-warning shadow-md flex items-center" aria-expanded="false">
                        Update 
                    </button>
                @endif
        </div>

   @if(empty($data->website_title) || is_null($data->website_title))
            <input type="text"  name="website_title" class="intro-y form-control py-3 px-4 box pr-10 mt-5 placeholder-theme-13" value="" placeholder="Website Title" required>
            @else
            <input type="text"  name="website_title" class="intro-y form-control py-3 px-4 box pr-10 mt-5 placeholder-theme-13" value="{{$data->website_title}}" placeholder="Title" required>
            @endif
   @csrf
    <div id="siteinfo" class="pos intro-y grid grid-cols-12 gap-5 mt-5">
   
        <!-- BEGIN: Post Content -->
        <div class="intro-y col-span-12 md:col-span-6 lg:col-span-6">
           
            <div class="post intro-y overflow-hidden box mt-5">
                <div class="post__content tab-content">
                    <div id="content" class="tab-pane p-5 active" role="tabpanel" aria-labelledby="content-tab">
                    <div class="mt-3 ">
                            <label class="form-label">Website Logo</label>
                            <div class="border-2 border-dashed dark:border-dark-5 rounded-md pt-4">
                                <div class="flex flex-wrap px-4">
                                    <div class="w-24 h-24 relative image-fit mb-5 mr-5 cursor-pointer zoom-in">
                                    @if(empty($data->logo) || is_null($data->logo) )
                                        <img id="logo_img" class="rounded-md get_img" alt="title image" src="" required>
                                        <input name="logo" type="hidden" id="logo">
                                    @else
                                        <img id="logo_img" class="rounded-md" alt="title image" src="{{$data->logo}}" required>
                                        <input name="logo" type="hidden" id="logo" value="{{$data->logo}}">
                                    @endif
                                        <div title="Remove this image?" class="remove tooltip w-5 h-5 flex items-center justify-center absolute rounded-full text-white bg-theme-6 right-0 top-0 -mr-2 -mt-2">
                                            <i data-feather="x" class="w-4 h-4"></i>
                                        </div>
                                    </div>
                                </div>
                                <div data-id="logo" class="px-4 pb-4 flex items-center cursor-pointer relative up_img_link">
                                    <i data-feather="image" class="w-4 h-4 mr-2"></i> <span class="text-theme-1 mr-1">Click to choose</span> Image from media
                                </div>
                            </div>
                        </div>
                        <div class="border border-gray-200 dark:border-dark-5 rounded-md p-5">
                            <div class="mt-5">
                                <div>
                                    <label for="post-form-7" class="form-label">Email</label>
                                    @if(empty($data->email) || is_null($data->email) )
                                    <input id="post-form-7" type="email" name="email" class="form-control" placeholder="Email" required>
                                    @else
                                    <input id="post-form-7" type="email" name="email" class="form-control" value="{{$data->email}}" placeholder="Email" required>
                                    @endif
                                </div>
                             
                            </div>
                        </div>  
                         <div class="border border-gray-200 dark:border-dark-5 rounded-md p-5">
                            <div class="mt-5">
                                <div>
                                <label for="post-form-7" class="form-label">Contact Number</label>
                                @if(empty($data->phone) || is_null($data->phone) )
                                    <input id="post-form-7" type="text" name="phone" class="form-control" placeholder="Contact number" required>
                                 @else
                                    <input id="post-form-7" type="text" name="phone" value="{{$data->phone}}" class="form-control" placeholder="Contact number" required>
                                 @endif   
                                </div>
                            </div>
                        </div>    
                        <div class="border border-gray-200 dark:border-dark-5 rounded-md p-5 ">
                            <div class="mt-5">
                                <div>
                                    <label for="post-form-7" class="form-label">Embed Google Map</label>
                                    @if(empty($data->gmap) || is_null($data->gmap) )
                                        <input id="post-form-7" type="text" name="gmap" class="form-control" placeholder="Gmap -> Search -> Share -> Embed -> Copy Html">
                                    @else
                                        <input id="post-form-7" type="text" name="gmap" value="{{$data->gmap}}" class="form-control" placeholder="Gmap -> Search -> Share -> Embed -> Copy Html">
                                    @endif
                                </div>
                            </div>
                        </div>
                         <div class="border border-gray-200 dark:border-dark-5 rounded-md p-5 ">
                            <div class="mt-5">
                                <div>
                                    <label for="post-form-7" class="form-label">Facebook link</label>
                                    @if(empty($data->facebook) || is_null($data->facebook) )
                                     <input id="post-form-7" type="text" name="facebook" class="form-control" placeholder="Facebook link">
                                    @else
                                    <input id="post-form-7" type="text" name="facebook" value="{{$data->facebook}}" class="form-control" placeholder="Facebook link">
                                    @endif
                                </div>
                            </div>
                        </div>  
                    </div>
                </div> 
            </div>
        </div>
        <!-- END: Post Content -->
            <!-- BEGIN: Post Content -->
            <div class="intro-y col-span-12 md:col-span-6 lg:col-span-6">
            <div class="post intro-y overflow-hidden box mt-5">
                <div class="post__content tab-content">
                    <div id="content" class="tab-pane p-5 active" role="tabpanel" aria-labelledby="content-tab">
                        <div class="mt-3 lg:col-span-8">
                            <label class="form-label">Site Favicon ( website icon )</label>
                            <div class="border-2 border-dashed dark:border-dark-5 rounded-md pt-4">
                                <div class="flex flex-wrap px-4">
                                    <div class="w-24 h-24 relative image-fit mb-5 mr-5 cursor-pointer zoom-in">
                                    @if(empty($data->favicon) || is_null($data->favicon) )
                                        <img id="favicon_img" class="rounded-md get_img " alt="title image" src="">
                                        <input name="favicon" type="hidden" id="favicon">
                                    @else
                                        <img id="favicon_img" class="rounded-md " alt="title image" src="{{$data->favicon}}">
                                        <input name="favicon" type="hidden" id="favicon" value="{{$data->favicon}}">
                                    @endif
                                        <div title="Remove this image?" class="remove tooltip w-5 h-5 flex items-center justify-center absolute rounded-full text-white bg-theme-6 right-0 top-0 -mr-2 -mt-2">
                                            <i data-feather="x" class="w-4 h-4"></i>
                                        </div>
                                    </div>
                                </div>
                                <div data-id="favicon" class="px-4 pb-4 flex items-center cursor-pointer relative up_img_link">
                                    <i data-feather="image" class="w-4 h-4 mr-2"></i> <span class="text-theme-1 mr-1">Click to choose</span> Image from media
                                </div>
                            </div>
                        </div>

                        <div class="border border-gray-200 dark:border-dark-5 rounded-md p-5">
                            <div class="mt-5">
                                <label for="post-form-7" class="form-label">Address</label>
                                @if(empty($data->add) || is_null($data->add) )
                                <textarea name="add" class="editor" placeholder="Add Address here" ></textarea>
                                @else
                                <div class="textDiv">
                                    <input name="add" class="form-control textareaPro" type="text" value="{{$data->add}}" >
                                    <textarea name="" class="editor" placeholder="Add Address here" value=""></textarea>
                                </div>
                                
                                @endif
                            </div>
                        </div>

                        <div class="border border-gray-200 dark:border-dark-5 rounded-md p-5 ">
                            <div class="mt-5">
                                <div>
                                    <label for="post-form-7" class="form-label">Instagram Link</label>
                                    @if(empty($data->instagram) || is_null($data->instagram) )
                                    <input id="post-form-7" type="text" name="instagram" class="form-control" placeholder="Instagram Link">
                                    @else
                                    <input id="post-form-7" type="text" name="instagram" class="form-control" value="{{$data->instagram}}" placeholder="Instagram Link">
                                    @endif
                                </div>
                            </div>
                        </div> 
                         <div class="border border-gray-200 dark:border-dark-5 rounded-md p-5 ">
                            <div class="mt-5">
                                <div>
                                    <label for="post-form-7" class="form-label">Linkedin Link</label>
                                    @if(empty($data->linkedin) || is_null($data->linkedin) )
                                    <input id="post-form-7" type="text" name="linkedin" class="form-control" placeholder="Linkedin Link">
                                    @else
                                    <input id="post-form-7" type="text" name="linkedin" class="form-control" value="{{$data->linkedin}}" placeholder="Linkedin Link">
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="border border-gray-200 dark:border-dark-5 rounded-md p-5 ">
                            <div class="mt-5">
                                <div>
                                    <label for="post-form-7" class="form-label">Twitter Link</label>
                                    @if(empty($data->twitter) || is_null($data->twitter) )
                                    <input id="post-form-7" type="text" name="twitter" class="form-control" placeholder="Twitter Link">
                                    @else
                                    <input id="post-form-7" type="text" value="{{$data->twitter}}" name="twitter" class="form-control" placeholder="Twitter Link">
                                    @endif
                                </div>
                            </div>
                        </div>

                    </div>
                </div> 
            </div>
        </div>
        <!------section 2--->
    </div>
    </form> 
@endsection