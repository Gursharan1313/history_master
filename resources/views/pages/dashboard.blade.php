@extends('../layout/' . $layout)



@section('subcontent')
    <div class="grid grid-cols-12 gap-6">
        <div class="col-span-12">
            <div class="grid grid-cols-12 gap-6">

                <!-- BEGIN: Weekly Top Products -->
                <div class="col-span-12 text-center mt-6">
                    <div class="wlc text-center">
                       <h1>Welcome to {{$siteInfo->website_title}}</h1>
                    </div>
                    <div class="inline-flex">
                        <img width="450" src="{{asset($siteInfo->logo)}}">
                    </div>
                </div>
                <!-- END: Weekly Top Products -->
            </div>
        </div>
    </div>
@endsection