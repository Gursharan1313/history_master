@extends('../layout/' . $layout)

@section('subcontent')
    <div class="intro-y flex flex-col sm:flex-row items-center mt-8">
        <h2 class="text-lg font-medium mr-auto">Home Page Edit</h2>
        <div class="w-full sm:w-auto flex mt-4 sm:mt-0">
           <!-- <button type="button" class="btn box text-gray-700 dark:text-gray-300 mr-2 flex items-center ml-auto sm:ml-0">
                <i class="w-4 h-4 mr-2" data-feather="eye"></i> Preview
            </button>-->
            <div class="dropdown">
            @if(empty($data->id) || is_null($data->id) )
                <button data-id="homepage" class="postData btn btn-primary shadow-md flex items-center" aria-expanded="false">
                    Save 
                </button>
            @else
            <button data-id="homepage" class="postData btn btn-warning shadow-md flex items-center" aria-expanded="false">
                    Update 
                </button>
            @endif
            </div>
        </div>
    </div>
       
   <form id="homepage" method="post" action="siteInfoData">
       
   @csrf
    <div id="siteinfo" class="pos intro-y grid grid-cols-12 gap-5 mt-5">
        <!-- BEGIN: Post Content -->
        <div class="intro-y col-span-12 md:col-span-6 lg:col-span-6">
            <div class="post intro-y overflow-hidden box mt-5">
                <div class="post__content tab-content">
                    <div id="content" class="tab-pane p-5 active" role="tabpanel" aria-labelledby="content-tab">
                        <div class="heading">Slider Content<hr></div>
                    <div class="mt-3 ">
                            <label class="form-label">Background Image</label>
                            <div class="border-2 border-dashed dark:border-dark-5 rounded-md pt-4">
                                <div class="flex flex-wrap px-4">
                                    <div class="w-24 h-24 relative image-fit mb-5 mr-5 cursor-pointer zoom-in">
                                    @if(empty($data->background) || is_null($data->background) )
                                        <img id="background_img" class="rounded-md get_img" alt="title image" src="">
                                        <input name="background" type="hidden" id="background">
                                    @else
                                        <img id="background_img" class="rounded-md" alt="title image" src="{{$data->background}}">
                                        <input name="background" type="hidden" id="background" value="{{$data->background}}">
                                    @endif
                                        <div title="Remove this image?" class="remove tooltip w-5 h-5 flex items-center justify-center absolute rounded-full text-white bg-theme-6 right-0 top-0 -mr-2 -mt-2">
                                            <i data-feather="x" class="w-4 h-4"></i>
                                        </div>
                                    </div>
                                </div>
                                <div data-id="background" class="px-4 pb-4 flex items-center cursor-pointer relative up_img_link">
                                    <i data-feather="image" class="w-4 h-4 mr-2"></i> <span class="text-theme-1 mr-1">Click to choose</span> Image from media
                                </div>
                            </div>
                        </div>
                        <div class="border border-gray-200 dark:border-dark-5 rounded-md">
                            <div class="mt-5">
                                <label for="post-form-7" class="form-label">Heading Content</label>
                                @if(empty($data->heading) || is_null($data->heading) )
                                <textarea name="add" class="editor" placeholder="Enter Heading here"></textarea>
                                @else
                                <div class="textDiv">
                                    <input name="add" class="form-control textareaPro" type="text" value="{{$data->heading}}" >
                                    <textarea name="" class="editor" placeholder="" value=""></textarea>
                                </div>
                                
                                @endif
                            </div>
                        </div>
                        <div class="mt-3 ">
                            <label class="form-label">Side Image</label>
                            <div class="border-2 border-dashed dark:border-dark-5 rounded-md pt-4">
                                <div class="flex flex-wrap px-4">
                                    <div class="w-24 h-24 relative image-fit mb-5 mr-5 cursor-pointer zoom-in">
                                    @if(empty($data->side_img) || is_null($data->side_img) )
                                        <img id="side_img_img" class="rounded-md get_img" alt="title image" src="">
                                        <input name="side_img" type="hidden" id="side_img">
                                    @else
                                        <img id="side_img_img" class="rounded-md" alt="title image" src="{{$data->side_img}}">
                                        <input name="side_img" type="hidden" id="side_img" value="{{$data->side_img}}">
                                    @endif
                                        <div title="Remove this image?" class="remove tooltip w-5 h-5 flex items-center justify-center absolute rounded-full text-white bg-theme-6 right-0 top-0 -mr-2 -mt-2">
                                            <i data-feather="x" class="w-4 h-4"></i>
                                        </div>
                                    </div>
                                </div>
                                <div data-id="side_img" class="px-4 pb-4 flex items-center cursor-pointer relative up_img_link">
                                    <i data-feather="image" class="w-4 h-4 mr-2"></i> <span class="text-theme-1 mr-1">Click to choose</span> Image from media
                                </div>
                            </div>
                        </div>
                        <div class="border border-gray-200 dark:border-dark-5 rounded-md">
                            <div class="mt-5">
                                <div>
                                    <label for="post-form-7" class="form-label">Video Link</label>
                                    @if(empty($data->video_link) || is_null($data->video_link) )
                                    <input id="post-form-7" type="text" name="video_link" class="form-control" placeholder="Video Link">
                                    @else
                                    <input id="post-form-7" type="text" name="video_link" class="form-control" value="{{$data->video_link}}" >
                                    @endif
                                </div>
                            </div>
                        </div> 
                        <div class="heading mt-5">3 Tabs (*Features)<hr></div> 
                         <div class="border border-gray-200 dark:border-dark-5 rounded-md ">
                            <div class="mt-5">
                                <div>
                                <label for="post-form-7" class="form-label">Tab 1</label>
                                @if(empty($data->tab1) || is_null($data->tab1) )
                                    <input id="post-form-7" type="text" name="tab1" class="form-control" placeholder="Tab 1 Heading">
                                 @else
                                    <input id="post-form-7" type="text" name="tab1" value="{{$data->tab1}}" class="form-control">
                                 @endif   
                                </div>
                            </div>
                        </div>    
                        <div class="border border-gray-200 dark:border-dark-5 rounded-md  ">
                            <div class="mt-5">
                                <div>
                                    <label for="post-form-7" class="form-label">Tab 2</label>
                                    @if(empty($data->tab2) || is_null($data->tab2) )
                                        <input id="post-form-7" type="text" name="tab2" class="form-control" placeholder="Tab 2 Heading">
                                    @else
                                        <input id="post-form-7" type="text" name="tab2" value="{{$data->tab2}}" class="form-control" >
                                    @endif
                                </div>
                            </div>
                        </div>
                         <div class="border border-gray-200 dark:border-dark-5 rounded-md ">
                            <div class="mt-5">
                                <div>
                                    <label for="post-form-7" class="form-label">Tab 3</label>
                                    @if(empty($data->tab3) || is_null($data->tab3) )
                                     <input id="post-form-7" type="text" name="tab3" class="form-control" placeholder="tab 3 Heading">
                                    @else
                                    <input id="post-form-7" type="text" name="tab3" value="{{$data->tab3}}" class="form-control">
                                    @endif
                                </div>
                            </div>
                        </div> 
                        <div class="heading mt-5">Brand Carousel<hr></div> 
                        <div class="mt-3 ">
                            <label class="form-label">Image 1</label>
                            <div class="border-2 border-dashed dark:border-dark-5 rounded-md pt-4">
                                <div class="flex flex-wrap px-4">
                                    <div class="w-24 h-24 relative image-fit mb-5 mr-5 cursor-pointer zoom-in">
                                    @if(empty($data->brand) || is_null($data->tab3) )
                                        <img id="background_img" class="rounded-md get_img" alt="title image" src="">
                                        <input name="background" type="hidden" id="background">
                                    @else
                                        <img id="background_img" class="rounded-md" alt="title image" src="{{$data->logo}}">
                                        <input name="background" type="hidden" id="background" value="{{$data->logo}}">
                                    @endif
                                        <div title="Remove this image?" class="remove tooltip w-5 h-5 flex items-center justify-center absolute rounded-full text-white bg-theme-6 right-0 top-0 -mr-2 -mt-2">
                                            <i data-feather="x" class="w-4 h-4"></i>
                                        </div>
                                    </div>
                                </div>
                                <div data-id="background" class="px-4 pb-4 flex items-center cursor-pointer relative up_img_link">
                                    <i data-feather="image" class="w-4 h-4 mr-2"></i> <span class="text-theme-1 mr-1">Click to choose</span> Image from media
                                </div>
                            </div>
                        </div><div class="mt-3 ">
                            <label class="form-label">Image 2</label>
                            <div class="border-2 border-dashed dark:border-dark-5 rounded-md pt-4">
                                <div class="flex flex-wrap px-4">
                                    <div class="w-24 h-24 relative image-fit mb-5 mr-5 cursor-pointer zoom-in">
                                    @if(empty($data->logo) || is_null($data->logo) )
                                        <img id="background_img" class="rounded-md get_img" alt="title image" src="">
                                        <input name="background" type="hidden" id="background">
                                    @else
                                        <img id="background_img" class="rounded-md" alt="title image" src="{{$data->logo}}">
                                        <input name="background" type="hidden" id="background" value="{{$data->logo}}">
                                    @endif
                                        <div title="Remove this image?" class="remove tooltip w-5 h-5 flex items-center justify-center absolute rounded-full text-white bg-theme-6 right-0 top-0 -mr-2 -mt-2">
                                            <i data-feather="x" class="w-4 h-4"></i>
                                        </div>
                                    </div>
                                </div>
                                <div data-id="background" class="px-4 pb-4 flex items-center cursor-pointer relative up_img_link">
                                    <i data-feather="image" class="w-4 h-4 mr-2"></i> <span class="text-theme-1 mr-1">Click to choose</span> Image from media
                                </div>
                            </div>
                        </div><div class="mt-3 ">
                            <label class="form-label">Image 3</label>
                            <div class="border-2 border-dashed dark:border-dark-5 rounded-md pt-4">
                                <div class="flex flex-wrap px-4">
                                    <div class="w-24 h-24 relative image-fit mb-5 mr-5 cursor-pointer zoom-in">
                                    @if(empty($data->logo) || is_null($data->logo) )
                                        <img id="background_img" class="rounded-md get_img" alt="title image" src="">
                                        <input name="background" type="hidden" id="background">
                                    @else
                                        <img id="background_img" class="rounded-md" alt="title image" src="{{$data->logo}}">
                                        <input name="background" type="hidden" id="background" value="{{$data->logo}}">
                                    @endif
                                        <div title="Remove this image?" class="remove tooltip w-5 h-5 flex items-center justify-center absolute rounded-full text-white bg-theme-6 right-0 top-0 -mr-2 -mt-2">
                                            <i data-feather="x" class="w-4 h-4"></i>
                                        </div>
                                    </div>
                                </div>
                                <div data-id="background" class="px-4 pb-4 flex items-center cursor-pointer relative up_img_link">
                                    <i data-feather="image" class="w-4 h-4 mr-2"></i> <span class="text-theme-1 mr-1">Click to choose</span> Image from media
                                </div>
                            </div>
                        </div><div class="mt-3 ">
                            <label class="form-label">Image 4</label>
                            <div class="border-2 border-dashed dark:border-dark-5 rounded-md pt-4">
                                <div class="flex flex-wrap px-4">
                                    <div class="w-24 h-24 relative image-fit mb-5 mr-5 cursor-pointer zoom-in">
                                    @if(empty($data->logo) || is_null($data->logo) )
                                        <img id="background_img" class="rounded-md get_img" alt="title image" src="">
                                        <input name="background" type="hidden" id="background">
                                    @else
                                        <img id="background_img" class="rounded-md" alt="title image" src="{{$data->logo}}">
                                        <input name="background" type="hidden" id="background" value="{{$data->logo}}">
                                    @endif
                                        <div title="Remove this image?" class="remove tooltip w-5 h-5 flex items-center justify-center absolute rounded-full text-white bg-theme-6 right-0 top-0 -mr-2 -mt-2">
                                            <i data-feather="x" class="w-4 h-4"></i>
                                        </div>
                                    </div>
                                </div>
                                <div data-id="background" class="px-4 pb-4 flex items-center cursor-pointer relative up_img_link">
                                    <i data-feather="image" class="w-4 h-4 mr-2"></i> <span class="text-theme-1 mr-1">Click to choose</span> Image from media
                                </div>
                            </div>
                        </div><div class="mt-3 ">
                            <label class="form-label">Image 5</label>
                            <div class="border-2 border-dashed dark:border-dark-5 rounded-md pt-4">
                                <div class="flex flex-wrap px-4">
                                    <div class="w-24 h-24 relative image-fit mb-5 mr-5 cursor-pointer zoom-in">
                                    @if(empty($data->logo) || is_null($data->logo) )
                                        <img id="background_img" class="rounded-md get_img" alt="title image" src="">
                                        <input name="background" type="hidden" id="background">
                                    @else
                                        <img id="background_img" class="rounded-md" alt="title image" src="{{$data->logo}}">
                                        <input name="background" type="hidden" id="background" value="{{$data->logo}}">
                                    @endif
                                        <div title="Remove this image?" class="remove tooltip w-5 h-5 flex items-center justify-center absolute rounded-full text-white bg-theme-6 right-0 top-0 -mr-2 -mt-2">
                                            <i data-feather="x" class="w-4 h-4"></i>
                                        </div>
                                    </div>
                                </div>
                                <div data-id="background" class="px-4 pb-4 flex items-center cursor-pointer relative up_img_link">
                                    <i data-feather="image" class="w-4 h-4 mr-2"></i> <span class="text-theme-1 mr-1">Click to choose</span> Image from media
                                </div>
                            </div>
                        </div><div class="mt-3 ">
                            <label class="form-label">Image 6</label>
                            <div class="border-2 border-dashed dark:border-dark-5 rounded-md pt-4">
                                <div class="flex flex-wrap px-4">
                                    <div class="w-24 h-24 relative image-fit mb-5 mr-5 cursor-pointer zoom-in">
                                    @if(empty($data->logo) || is_null($data->logo) )
                                        <img id="background_img" class="rounded-md get_img" alt="title image" src="">
                                        <input name="background" type="hidden" id="background">
                                    @else
                                        <img id="background_img" class="rounded-md" alt="title image" src="{{$data->logo}}">
                                        <input name="background" type="hidden" id="background" value="{{$data->logo}}">
                                    @endif
                                        <div title="Remove this image?" class="remove tooltip w-5 h-5 flex items-center justify-center absolute rounded-full text-white bg-theme-6 right-0 top-0 -mr-2 -mt-2">
                                            <i data-feather="x" class="w-4 h-4"></i>
                                        </div>
                                    </div>
                                </div>
                                <div data-id="background" class="px-4 pb-4 flex items-center cursor-pointer relative up_img_link">
                                    <i data-feather="image" class="w-4 h-4 mr-2"></i> <span class="text-theme-1 mr-1">Click to choose</span> Image from media
                                </div>
                            </div>
                        </div>
                        <div class="heading mt-5">Testimonials<hr></div>
                        <div class="border border-gray-200 dark:border-dark-5 rounded-md ">
                            <div class="mt-5">
                                <div>
                                    <label for="post-form-7" class="form-label">Client 1</label>
                                    @if(empty($data->linkedin) || is_null($data->linkedin) )
                                    <input id="post-form-7" type="text" name="linkedin" class="form-control" placeholder="Linkedin Link">
                                    @else
                                    <input id="post-form-7" type="text" name="linkedin" class="form-control" value="{{$data->linkedin}}" placeholder="Linkedin Link">
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="border border-gray-200 dark:border-dark-5 rounded-md ">
                            <div class="mt-5">
                                <div>
                                    <label for="post-form-7" class="form-label">Designation</label>
                                    @if(empty($data->linkedin) || is_null($data->linkedin) )
                                    <input id="post-form-7" type="text" name="linkedin" class="form-control" placeholder="Linkedin Link">
                                    @else
                                    <input id="post-form-7" type="text" name="linkedin" class="form-control" value="{{$data->linkedin}}" placeholder="Linkedin Link">
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="border border-gray-200 dark:border-dark-5 rounded-md">
                            <div class="mt-5">
                                <label for="post-form-7" class="form-label">Testimonial 1</label>
                                @if(empty($data->add) || is_null($data->add) )
                                <textarea name="add" class="editor" placeholder="Add Address here"></textarea>
                                @else
                                <div class="textDiv">
                                    <input name="add" class="form-control textareaPro" type="text" value="{{$data->add}}" >
                                    <textarea name="" class="editor" placeholder="Add Address here" value=""></textarea>
                                </div>
                                
                                @endif
                            </div>
                        </div>
                        <div class="border border-gray-200 dark:border-dark-5 rounded-md ">
                            <div class="mt-5">
                                <div>
                                    <label for="post-form-7" class="form-label">Client 3</label>
                                    @if(empty($data->linkedin) || is_null($data->linkedin) )
                                    <input id="post-form-7" type="text" name="linkedin" class="form-control" placeholder="Linkedin Link">
                                    @else
                                    <input id="post-form-7" type="text" name="linkedin" class="form-control" value="{{$data->linkedin}}" placeholder="Linkedin Link">
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="border border-gray-200 dark:border-dark-5 rounded-md ">
                            <div class="mt-5">
                                <div>
                                    <label for="post-form-7" class="form-label">Designation</label>
                                    @if(empty($data->linkedin) || is_null($data->linkedin) )
                                    <input id="post-form-7" type="text" name="linkedin" class="form-control" placeholder="Linkedin Link">
                                    @else
                                    <input id="post-form-7" type="text" name="linkedin" class="form-control" value="{{$data->linkedin}}" placeholder="Linkedin Link">
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="border border-gray-200 dark:border-dark-5 rounded-md">
                            <div class="mt-5">
                                <label for="post-form-7" class="form-label">Testimonial 3</label>
                                @if(empty($data->add) || is_null($data->add) )
                                <textarea name="add" class="editor" placeholder="Add Address here"></textarea>
                                @else
                                <div class="textDiv">
                                    <input name="add" class="form-control textareaPro" type="text" value="{{$data->add}}" >
                                    <textarea name="" class="editor" placeholder="Add Address here" value=""></textarea>
                                </div>
                                
                                @endif
                            </div>
                        </div>
                        <div class="border border-gray-200 dark:border-dark-5 rounded-md ">
                            <div class="mt-5">
                                <div>
                                    <label for="post-form-7" class="form-label">Client 5</label>
                                    @if(empty($data->linkedin) || is_null($data->linkedin) )
                                    <input id="post-form-7" type="text" name="linkedin" class="form-control" placeholder="Linkedin Link">
                                    @else
                                    <input id="post-form-7" type="text" name="linkedin" class="form-control" value="{{$data->linkedin}}" placeholder="Linkedin Link">
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="border border-gray-200 dark:border-dark-5 rounded-md ">
                            <div class="mt-5">
                                <div>
                                    <label for="post-form-7" class="form-label">Designation</label>
                                    @if(empty($data->linkedin) || is_null($data->linkedin) )
                                    <input id="post-form-7" type="text" name="linkedin" class="form-control" placeholder="Linkedin Link">
                                    @else
                                    <input id="post-form-7" type="text" name="linkedin" class="form-control" value="{{$data->linkedin}}" placeholder="Linkedin Link">
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="border border-gray-200 dark:border-dark-5 rounded-md">
                            <div class="mt-5">
                                <label for="post-form-7" class="form-label">Testimonial 5</label>
                                @if(empty($data->add) || is_null($data->add) )
                                <textarea name="add" class="editor" placeholder="Add Address here"></textarea>
                                @else
                                <div class="textDiv">
                                    <input name="add" class="form-control textareaPro" type="text" value="{{$data->add}}" >
                                    <textarea name="" class="editor" placeholder="Add Address here" value=""></textarea>
                                </div>
                                
                                @endif
                            </div>
                        </div>


                    </div>
                </div> 
            </div>
        </div>
        <!-- END: Post Content -->
            <!-- BEGIN: Post Content -->
            <div class="intro-y col-span-12 md:col-span-6 lg:col-span-6">
            <div class="post intro-y overflow-hidden box mt-5">
                <div class="post__content tab-content">
                    <div id="content" class="tab-pane p-5 active" role="tabpanel" aria-labelledby="content-tab">
                    <div class="heading">About us section<hr></div>    
                        <div class="mt-3 lg:col-span-8">
                            <label class="form-label">Side image</label>
                            <div class="border-2 border-dashed dark:border-dark-5 rounded-md pt-4">
                                <div class="flex flex-wrap px-4">
                                    <div class="w-24 h-24 relative image-fit mb-5 mr-5 cursor-pointer zoom-in">
                                    @if(empty($data->favicon) || is_null($data->favicon) )
                                        <img id="favicon_img" class="rounded-md get_img " alt="title image" src="">
                                        <input name="favicon" type="hidden" id="favicon">
                                    @else
                                        <img id="favicon_img" class="rounded-md " alt="title image" src="{{$data->favicon}}">
                                        <input name="favicon" type="hidden" id="favicon" value="{{$data->favicon}}">
                                    @endif
                                        <div title="Remove this image?" class="remove tooltip w-5 h-5 flex items-center justify-center absolute rounded-full text-white bg-theme-6 right-0 top-0 -mr-2 -mt-2">
                                            <i data-feather="x" class="w-4 h-4"></i>
                                        </div>
                                    </div>
                                </div>
                                <div data-id="favicon" class="px-4 pb-4 flex items-center cursor-pointer relative up_img_link">
                                    <i data-feather="image" class="w-4 h-4 mr-2"></i> <span class="text-theme-1 mr-1">Click to choose</span> Image from media
                                </div>
                            </div>
                        </div>

                        <div class="border border-gray-200 dark:border-dark-5 rounded-md">
                            <div class="mt-5">
                                <label for="post-form-7" class="form-label">About us content</label>
                                @if(empty($data->add) || is_null($data->add) )
                                <textarea name="add" class="editor" placeholder="Add Address here"></textarea>
                                @else
                                <div class="textDiv">
                                    <input name="add" class="form-control textareaPro" type="text" value="{{$data->add}}" >
                                    <textarea name="" class="editor" placeholder="Add Address here" value=""></textarea>
                                </div>
                                
                                @endif
                            </div>
                        </div>
                        <div class="heading mt-5">Highlighted Box<hr></div>  
                        <div class="border border-gray-200 dark:border-dark-5 rounded-md ">
                            <div class="mt-5">
                                <div>
                                    <label for="post-form-7" class="form-label">Title</label>
                                    @if(empty($data->instagram) || is_null($data->instagram) )
                                    <input id="post-form-7" type="text" name="instagram" class="form-control" placeholder="Instagram Link">
                                    @else
                                    <input id="post-form-7" type="text" name="instagram" class="form-control" value="{{$data->instagram}}" placeholder="Instagram Link">
                                    @endif
                                </div>
                            </div>
                        </div> 
                         <div class="border border-gray-200 dark:border-dark-5 rounded-md ">
                            <div class="mt-5">
                                <div>
                                    <label for="post-form-7" class="form-label">Heading</label>
                                    @if(empty($data->linkedin) || is_null($data->linkedin) )
                                    <input id="post-form-7" type="text" name="linkedin" class="form-control" placeholder="Linkedin Link">
                                    @else
                                    <input id="post-form-7" type="text" name="linkedin" class="form-control" value="{{$data->linkedin}}" placeholder="Linkedin Link">
                                    @endif
                                </div>
                            </div>
                        </div> 
                        <div class="border border-gray-200 dark:border-dark-5 rounded-md">
                            <div class="mt-5">
                                <label for="post-form-7" class="form-label">Box 1</label>
                                @if(empty($data->add) || is_null($data->add) )
                                <textarea name="add" class="editor" placeholder="Add Address here"></textarea>
                                @else
                                <div class="textDiv">
                                    <input name="add" class="form-control textareaPro" type="text" value="{{$data->add}}" >
                                    <textarea name="" class="editor" placeholder="Add Address here" value=""></textarea>
                                </div>
                                @endif
                            </div>
                        </div>
                        <div class="border border-gray-200 dark:border-dark-5 rounded-md ">
                            <div class="mt-5">
                                <div>
                                    <label for="post-form-7" class="form-label">Link 1</label>
                                    @if(empty($data->linkedin) || is_null($data->linkedin) )
                                    <input id="post-form-7" type="text" name="linkedin" class="form-control" placeholder="Linkedin Link">
                                    @else
                                    <input id="post-form-7" type="text" name="linkedin" class="form-control" value="{{$data->linkedin}}" placeholder="Linkedin Link">
                                    @endif
                                </div>
                            </div>
                        </div> 
                        <div class="border border-gray-200 dark:border-dark-5 rounded-md">
                            <div class="mt-5">
                                <label for="post-form-7" class="form-label">Box 2</label>
                                @if(empty($data->add) || is_null($data->add) )
                                <textarea name="add" class="editor" placeholder="Add Address here"></textarea>
                                @else
                                <div class="textDiv">
                                    <input name="add" class="form-control textareaPro" type="text" value="{{$data->add}}" >
                                    <textarea name="" class="editor" placeholder="Add Address here" value=""></textarea>
                                </div>
                                
                                @endif
                            </div>
                        </div>   
                        <div class="border border-gray-200 dark:border-dark-5 rounded-md ">
                            <div class="mt-5">
                                <div>
                                    <label for="post-form-7" class="form-label">Link 2</label>
                                    @if(empty($data->linkedin) || is_null($data->linkedin) )
                                    <input id="post-form-7" type="text" name="linkedin" class="form-control" placeholder="Linkedin Link">
                                    @else
                                    <input id="post-form-7" type="text" name="linkedin" class="form-control" value="{{$data->linkedin}}" placeholder="Linkedin Link">
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="border border-gray-200 dark:border-dark-5 rounded-md">
                            <div class="mt-5">
                                <label for="post-form-7" class="form-label">Box 3</label>
                                @if(empty($data->add) || is_null($data->add) )
                                <textarea name="add" class="editor" placeholder="Add Address here"></textarea>
                                @else
                                <div class="textDiv">
                                    <input name="add" class="form-control textareaPro" type="text" value="{{$data->add}}" >
                                    <textarea name="" class="editor" placeholder="Add Address here" value=""></textarea>
                                </div>
                                
                                @endif
                            </div>
                        </div> 
                        <div class="border border-gray-200 dark:border-dark-5 rounded-md ">
                            <div class="mt-5">
                                <div>
                                    <label for="post-form-7" class="form-label">Link 3</label>
                                    @if(empty($data->linkedin) || is_null($data->linkedin) )
                                    <input id="post-form-7" type="text" name="linkedin" class="form-control" placeholder="Linkedin Link">
                                    @else
                                    <input id="post-form-7" type="text" name="linkedin" class="form-control" value="{{$data->linkedin}}" placeholder="Linkedin Link">
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="border border-gray-200 dark:border-dark-5 rounded-md">
                            <div class="mt-5">
                                <label for="post-form-7" class="form-label">Box 4</label>
                                @if(empty($data->add) || is_null($data->add) )
                                <textarea name="add" class="editor" placeholder="Add Address here"></textarea>
                                @else
                                <div class="textDiv">
                                    <input name="add" class="form-control textareaPro" type="text" value="{{$data->add}}" >
                                    <textarea name="" class="editor" placeholder="Add Address here" value=""></textarea>
                                </div>
                                
                                @endif
                            </div>
                        </div>
                        <div class="border border-gray-200 dark:border-dark-5 rounded-md ">
                            <div class="mt-5">
                                <div>
                                    <label for="post-form-7" class="form-label">Link 4</label>
                                    @if(empty($data->linkedin) || is_null($data->linkedin) )
                                    <input id="post-form-7" type="text" name="linkedin" class="form-control" placeholder="Linkedin Link">
                                    @else
                                    <input id="post-form-7" type="text" name="linkedin" class="form-control" value="{{$data->linkedin}}" placeholder="Linkedin Link">
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="heading mt-20">Testimonials<hr></div>
                        <div class="border border-gray-200 dark:border-dark-5 rounded-md ">
                            <div class="mt-5">
                                <div>
                                    <label for="post-form-7" class="form-label">Client 2</label>
                                    @if(empty($data->linkedin) || is_null($data->linkedin) )
                                    <input id="post-form-7" type="text" name="linkedin" class="form-control" placeholder="Linkedin Link">
                                    @else
                                    <input id="post-form-7" type="text" name="linkedin" class="form-control" value="{{$data->linkedin}}" placeholder="Linkedin Link">
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="border border-gray-200 dark:border-dark-5 rounded-md ">
                            <div class="mt-5">
                                <div>
                                    <label for="post-form-7" class="form-label">Designation</label>
                                    @if(empty($data->linkedin) || is_null($data->linkedin) )
                                    <input id="post-form-7" type="text" name="linkedin" class="form-control" placeholder="Linkedin Link">
                                    @else
                                    <input id="post-form-7" type="text" name="linkedin" class="form-control" value="{{$data->linkedin}}" placeholder="Linkedin Link">
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="border border-gray-200 dark:border-dark-5 rounded-md">
                            <div class="mt-5">
                                <label for="post-form-7" class="form-label">Testimonial 2</label>
                                @if(empty($data->add) || is_null($data->add) )
                                <textarea name="add" class="editor" placeholder="Add Address here"></textarea>
                                @else
                                <div class="textDiv">
                                    <input name="add" class="form-control textareaPro" type="text" value="{{$data->add}}" >
                                    <textarea name="" class="editor" placeholder="Add Address here" value=""></textarea>
                                </div>
                                
                                @endif
                            </div>
                        </div>

                        <div class="border border-gray-200 dark:border-dark-5 rounded-md ">
                            <div class="mt-5">
                                <div>
                                    <label for="post-form-7" class="form-label">Client 4</label>
                                    @if(empty($data->linkedin) || is_null($data->linkedin) )
                                    <input id="post-form-7" type="text" name="linkedin" class="form-control" placeholder="Linkedin Link">
                                    @else
                                    <input id="post-form-7" type="text" name="linkedin" class="form-control" value="{{$data->linkedin}}" placeholder="Linkedin Link">
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="border border-gray-200 dark:border-dark-5 rounded-md ">
                            <div class="mt-5">
                                <div>
                                    <label for="post-form-7" class="form-label">Designation</label>
                                    @if(empty($data->linkedin) || is_null($data->linkedin) )
                                    <input id="post-form-7" type="text" name="linkedin" class="form-control" placeholder="Linkedin Link">
                                    @else
                                    <input id="post-form-7" type="text" name="linkedin" class="form-control" value="{{$data->linkedin}}" placeholder="Linkedin Link">
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="border border-gray-200 dark:border-dark-5 rounded-md">
                            <div class="mt-5">
                                <label for="post-form-7" class="form-label">Testimonial 4</label>
                                @if(empty($data->add) || is_null($data->add) )
                                <textarea name="add" class="editor" placeholder="Add Address here"></textarea>
                                @else
                                <div class="textDiv">
                                    <input name="add" class="form-control textareaPro" type="text" value="{{$data->add}}" >
                                    <textarea name="" class="editor" placeholder="Add Address here" value=""></textarea>
                                </div>
                                
                                @endif
                            </div>
                        </div>
                        <div class="border border-gray-200 dark:border-dark-5 rounded-md ">
                            <div class="mt-5">
                                <div>
                                    <label for="post-form-7" class="form-label">Client 6</label>
                                    @if(empty($data->linkedin) || is_null($data->linkedin) )
                                    <input id="post-form-7" type="text" name="linkedin" class="form-control" placeholder="Linkedin Link">
                                    @else
                                    <input id="post-form-7" type="text" name="linkedin" class="form-control" value="{{$data->linkedin}}" placeholder="Linkedin Link">
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="border border-gray-200 dark:border-dark-5 rounded-md ">
                            <div class="mt-5">
                                <div>
                                    <label for="post-form-7" class="form-label">Designation</label>
                                    @if(empty($data->linkedin) || is_null($data->linkedin) )
                                    <input id="post-form-7" type="text" name="linkedin" class="form-control" placeholder="Linkedin Link">
                                    @else
                                    <input id="post-form-7" type="text" name="linkedin" class="form-control" value="{{$data->linkedin}}" placeholder="Linkedin Link">
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="border border-gray-200 dark:border-dark-5 rounded-md">
                            <div class="mt-5">
                                <label for="post-form-7" class="form-label">Testimonial 6</label>
                                @if(empty($data->add) || is_null($data->add) )
                                <textarea name="add" class="editor" placeholder="Add Address here"></textarea>
                                @else
                                <div class="textDiv">
                                    <input name="add" class="form-control textareaPro" type="text" value="{{$data->add}}" >
                                    <textarea name="" class="editor" placeholder="Add Address here" value=""></textarea>
                                </div>
                                
                                @endif
                            </div>
                        </div>

                    </div>
                </div> 
            </div>
        </div>
        <!------section 2--->
    </div>
    </form> 
@endsection