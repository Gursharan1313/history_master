@extends('../layout/' . $layout)


@section('subcontent')
    <div class="grid grid-cols-12 gap-6 mt-8">
        <div class="col-span-12 lg:col-span-3 xxl:col-span-2">
            <h2 class="intro-y text-lg font-medium mr-auto mt-2">File Manager</h2>
        </div>
        <div class="col-span-12 lg:col-span-12 xxl:col-span-12">
            <!-- BEGIN: File Manager Filter -->
            <div class="intro-y flex flex-col-reverse sm:flex-row items-center">
                <div class="w-full sm:w-auto flex">
                    <button data-id="fileUp" class="btn btn-primary shadow-md mr-2 popup" data-animate="slide">Upload New Files</button>
                </div>
            </div>
            <div id="fileUp" class="mt-5 mb-5">
               
                <div class="flex-column block"> 
                    <form data-file-types="image/jpeg|image/png|image/jpg|video/mp4|video/WebM|video/avi|video/wav|application/pdf" action="file-upload" class="dropzone">
                    @csrf
                        <div class="fallback">
                            <input name="file" type="file" />
                        </div>
                        <div class="dz-message" data-dz-message>
                            <div class="text-lg font-medium">Drop files here or click to upload.</div>
                            <div class="text-gray-600">
                            You can Select Multiple Files to upload.
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- END: File Manager Filter-->
             <!-- BEGIN: Directory & Files -->
            <div class="intro-y grid grid-cols-12 gap-3 sm:gap-6 mt-5">
                @foreach ($file as $file)
                    <div class="intro-y col-span-4 sm:col-span-3 md:col-span-3 xxl:col-span-2">
                        <div class="file box rounded-md px-5 pt-8 pb-5 px-3 sm:px-5 relative zoom-in">
                            @if ($file['location'] == 'images') 
                                <a href="{{Storage::url($file['file_name'])}}" target="_blank" class="w-3/5 file__icon file__icon--image mx-auto">
                                    <div class="file__icon--image__preview image-fit">
                                        <img alt="image" src="{{Storage::url($file['file_name'])}}">
                                    </div>
                                </a>
                            @elseif($file['location'] == 'videos')
                            <a href="{{Storage::url($file['file_name'])}}" target="_blank" class="w-3/5 mx-auto">
                                    <div class="file__icon--image__preview image-fit pb-2">
                                        <video width="100%" height="240" controls>
                                            <source src="{{Storage::url($file['file_name'])}}" type="video/{{$file['file_type']}}">
                                            Your browser does not support the video tag.
                                        </video>
                                    </div>
                                </a>
                            @else
                                <a href="{{Storage::url($file['file_name'])}}" target="_blank" class="w-3/5 file__icon file__icon--file mx-auto">
                                    <div class="file__icon__file-name">{{$file['file_type']}}</div>
                                </a>
                            @endif
                            <a href="" class="block font-medium mt-4 text-center truncate">{{$file['file_name']}}</a>
                            <div class="text-gray-600 text-xs text-center mt-0.5">{{$file['file_size']}}</div>
                            <div class="absolute top-0 right-0 mr-2 mt-2 dropdown ml-auto">
                                <a class="dropdown-toggle w-5 h-5 block" href="javascript:;" aria-expanded="false">
                                    <i data-feather="more-vertical" class="w-5 h-5 text-gray-600"></i>
                                </a>
                                <div class="dropdown-menu w-40">
                                    <div class="dropdown-menu__content box dark:bg-dark-1 p-2">
                                        <a href="{{Storage::url($file['file_name'])}}" target="_blank" class="flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2  rounded-md">
                                            <i data-feather="copy" class="w-4 h-4 mr-2"></i> Open in new tab
                                        </a>
                                        <a data-id="{{$file['id']}}" class="DelImg flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2  rounded-md">
                                            <i data-feather="trash" class="w-4 h-4 mr-2"></i> Delete
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <!-- END: Directory & Files -->
        </div>
    </div>
@endsection