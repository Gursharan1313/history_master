@extends('../layout/' . $layout)
@section('subcontent')
    <div class="intro-y flex flex-col sm:flex-row items-center mt-8">
        <h2 class="text-lg font-medium mr-auto">Blogs</h2>
        <div class="w-full sm:w-auto flex mt-4 sm:mt-0">
            <a href="{{route('post')}}"><button class="btn btn-primary shadow-md mr-2">Add New Post</button></a>
        </div>
    </div>
    <div class="intro-y grid grid-cols-12 gap-6 mt-5">
        <!-- BEGIN: Blog Layout -->
        @foreach($post as $post)
            <div class="intro-y col-span-12 md:col-span-6 xl:col-span-4 box">
                        <div class="flex items-center border-b border-gray-200 dark:border-dark-5 px-5 py-4">
                            <div class="w-10 h-10 flex-none image-fit">
                                <img alt="admin" class="rounded-full" src="dist/images/profile-13.jpg">
                            </div>
                            <div class="ml-3 mr-auto">
                                <a href="" class="font-medium">{{$post->written_by}}</a> 
                                <div class="flex text-gray-600 truncate text-xs mt-0.5"> <a class="text-theme-1 dark:text-theme-10 inline-block truncate" href="#">{{$post->caption}}</a> <span class="mx-1">•</span> {{$post->post_date}} </div>
                            </div>
                            <div class="dropdown ml-3">
                                <a href="javascript:;" class="dropdown-toggle w-5 h-5 text-gray-600 dark:text-gray-300" aria-expanded="false"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-more-vertical w-4 h-4"><circle cx="12" cy="12" r="1"></circle><circle cx="12" cy="5" r="1"></circle><circle cx="12" cy="19" r="1"></circle></svg> </a>
                                <div class="dropdown-menu w-40">
                                    <div class="dropdown-menu__content box dark:bg-dark-1 p-2">
                                        <a href="" class="flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-2 w-4 h-4 mr-2"><path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path></svg> Edit Post </a>
                                        <a href="#" data-id="{{$post->id}}" class="delBlog flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash w-4 h-4 mr-2"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path></svg> Delete Post </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="p-5">
                            <div class="h-40 xxl:h-56 image-fit">
                                <img alt="Blog image" class="rounded-md" src="{{$post['file_name']}}">
                            </div>
                            <a href="#" class="block font-medium text-base mt-5">{{$post->title}}</a> 
                            <div class="text-gray-700 dark:text-gray-600 mt-2">@php 
                                echo substr($post->body,0,120);
                            @endphp
                            </div>
                        </div>
                        <!--<div class="flex items-center px-5 py-3 border-t border-gray-200 dark:border-dark-5">
                            <div class="intro-x flex mr-2">
                                <div class="intro-x w-8 h-8 image-fit">
                                    <img alt="Midone Tailwind HTML Admin Template" class="rounded-full border border-white zoom-in tooltip" src="dist/images/profile-13.jpg">
                                </div>
                                <div class="intro-x w-8 h-8 image-fit -ml-4">
                                    <img alt="Midone Tailwind HTML Admin Template" class="rounded-full border border-white zoom-in tooltip" src="dist/images/profile-3.jpg">
                                </div>
                                <div class="intro-x w-8 h-8 image-fit -ml-4">
                                    <img alt="Midone Tailwind HTML Admin Template" class="rounded-full border border-white zoom-in tooltip" src="dist/images/profile-5.jpg">
                                </div>
                            </div>
                            <a href="" class="intro-x w-8 h-8 flex items-center justify-center rounded-full bg-theme-14 dark:bg-dark-5 dark:text-gray-300 text-theme-10 ml-auto tooltip"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-share-2 w-3 h-3"><circle cx="18" cy="5" r="3"></circle><circle cx="6" cy="12" r="3"></circle><circle cx="18" cy="19" r="3"></circle><line x1="8.59" y1="13.51" x2="15.42" y2="17.49"></line><line x1="15.41" y1="6.51" x2="8.59" y2="10.49"></line></svg> </a>
                            <a href="" class="intro-x w-8 h-8 flex items-center justify-center rounded-full bg-theme-1 text-white ml-2 tooltip"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-share w-3 h-3"><path d="M4 12v8a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2v-8"></path><polyline points="16 6 12 2 8 6"></polyline><line x1="12" y1="2" x2="12" y2="15"></line></svg> </a>
                        </div>-->
                        <div class="px-5 pt-3 pb-5 border-t border-gray-200 dark:border-dark-5">
                            <div class="w-full flex text-gray-600 text-xs sm:text-sm">
                                <div class="mr-2"> Comments: <span class="font-medium">99</span> </div>
                                <div class="mr-2"> Views: <span class="font-medium">27k</span> </div>
                                <div class="ml-auto"> Likes: <span class="font-medium">27k</span> </div>
                            </div>
                            <div class="w-full flex items-center mt-3">
                                <div class="w-8 h-8 flex-none image-fit mr-3">
                                    <img alt="Midone Tailwind HTML Admin Template" class="rounded-full" src="dist/images/profile-13.jpg">
                                </div>
                                <div class="flex-1 relative text-gray-700">
                                    <input type="text" class="form-control form-control-rounded border-transparent bg-gray-200 pr-10 placeholder-theme-13" placeholder="Post a comment...">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-smile w-4 h-4 absolute my-auto inset-y-0 mr-3 right-0"><circle cx="12" cy="12" r="10"></circle><path d="M8 14s1.5 2 4 2 4-2 4-2"></path><line x1="9" y1="9" x2="9.01" y2="9"></line><line x1="15" y1="9" x2="15.01" y2="9"></line></svg> 
                                </div>
                            </div>
                        </div>
                    </div>
            @endforeach
        <!-- END: Blog Layout -->
        <!-- BEGIN: Pagiantion 
        <div class="intro-y col-span-12 flex flex-wrap sm:flex-row sm:flex-nowrap items-center">
            <ul class="pagination">
                <li>
                    <a class="pagination__link" href="">
                        <i class="w-4 h-4" data-feather="chevrons-left"></i>
                    </a>
                </li>
                <li>
                    <a class="pagination__link" href="">
                        <i class="w-4 h-4" data-feather="chevron-left"></i>
                    </a>
                </li>
                <li>
                    <a class="pagination__link" href="">...</a>
                </li>
                <li>
                    <a class="pagination__link" href="">1</a>
                </li>
                <li>
                    <a class="pagination__link pagination__link--active" href="">2</a>
                </li>
                <li>
                    <a class="pagination__link" href="">3</a>
                </li>
                <li>
                    <a class="pagination__link" href="">...</a>
                </li>
                <li>
                    <a class="pagination__link" href="">
                        <i class="w-4 h-4" data-feather="chevron-right"></i>
                    </a>
                </li>
                <li>
                    <a class="pagination__link" href="">
                        <i class="w-4 h-4" data-feather="chevrons-right"></i>
                    </a>
                </li>
            </ul>
            <select class="w-20 form-select box mt-3 sm:mt-0">
                <option>10</option>
                <option>25</option>
                <option>35</option>
                <option>50</option>
            </select>
        </div>
        END: Pagiantion -->
    </div>
@endsection