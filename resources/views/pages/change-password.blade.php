@extends('../layout/' . $layout)

@section('subhead')
    <title>Update Profile - Midone - Tailwind HTML Admin Template</title>
@endsection

@section('subcontent')
    <div class="intro-y flex items-center mt-8">
        <h2 class="text-lg font-medium mr-auto">Change Password</h2>
    </div>
    <div class="grid grid-cols-12 gap-6">
        <div class="col-span-12 lg:col-span-8 xxl:col-span-9">
            <!-- BEGIN: Change Password -->
    <form method="post" action="changepassword" id="changepass">
        @csrf
        @foreach($id as $id)
        <input id="UserId" name="id" type="hidden" class="form-control" value="{{$id}}">
        @endforeach
            <div class="intro-y box lg:mt-5">
                <div class="flex items-center p-5 border-b border-gray-200 dark:border-dark-5">
                    <h2 class="font-medium text-base mr-auto">Change Password</h2>
                </div>
                <div class="p-5">
                    <div>
                        <label for="old-pass" class="form-label">Old Password</label>
                        <input id="old-pass" type="password" class="form-control" placeholder="Input text" required>
                    </div>
                    <div class="mt-3">
                        <label for="change-password-form-2" class="form-label">New Password</label>
                        <input id="change-password-form-1" type="password" name="password" class="form-control" placeholder="Input text" required>
                    </div>
                    <div class="mt-3">
                        <label for="change-password-form-2" class="form-label">Confirm New Password</label>
                        <input id="change-password-form-2" type="password" class="form-control" placeholder="Input text" required>
                    </div>
                    <input type="submit" class="changepass btn btn-primary mt-4" value="Change Password">
                </div>
            </div>
    </form>
            <!-- END: Change Password -->
        </div>
    </div>
@endsection