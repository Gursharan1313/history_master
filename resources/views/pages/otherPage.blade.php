@extends('../layout/' . $layout)

@section('subcontent')
<script src="{{asset('dist/ckeditor/ckeditor.js')}}"></script>
	<script src="{{asset('dist/ckeditor/sample.js')}}"></script>
    <div class="intro-y flex flex-col sm:flex-row items-center mt-8">
        <h2 class="text-lg font-medium mr-auto">Add New Page</h2>
        @if(empty($page_content) || is_null($page_content))
        @else
        <div class="w-full sm:w-auto flex mt-4 sm:mt-0">
           <!-- <button type="button" class="btn box text-gray-700 dark:text-gray-300 mr-2 flex items-center ml-auto sm:ml-0">
                <i class="w-4 h-4 mr-2" data-feather="eye"></i> Preview
            </button>-->
            <div class="dropdown">
                <button data-id="{{$page_content->id}}" class="delPage btn btn-danger shadow-md flex items-center" aria-expanded="false">
                    Delete 
                </button>
            </div>
        </div>
        @endif
    </div>
    @if(!empty($message))
        @if($message != "Fail")
        <div class="message">
            <div>{{$message}}</div>
        </div>
        @else
        <div class="fail">
            <div>{{$message}}</div>
        </div>
        @endif
    @endif
    <div class="intro-y flex flex-col sm:flex-row items-center mt-8">
        @foreach($pages as $page)  
            @if($page->status == "on")
            <a href="/otherpage/{{$page->id}}"><button class="btn btn-success mr-2">{{$page->page_title}}</button></a>
            @else
            <a href="/otherpage/{{$page->id}}"><button class="btn btn-danger mr-2">{{$page->page_title}}</button></a>
            @endif
        @endforeach
    </div>
    <form id="othePageData" method="post" action="post-othePage-data">
   @csrf
    <div id="post_blog" class="pos intro-y grid grid-cols-6 gap-5 mt-5">
        <!-- BEGIN: Post Content -->
        <div class="intro-y col-span-12 lg:col-span-8">
            
            @if(empty($page_content) || is_null($page_content))
            @else
            <input type="hidden"  name="page_id" value="{{$page_content->id}}" placeholder="Title">
            @endif
            @if(empty($page_content) || is_null($page_content))
            <input type="text"  name="page_title" class="intro-y form-control py-3 px-4 box pr-10 placeholder-theme-13" value="" placeholder="Title">
            @else
            <input type="text"  name="page_title" class="intro-y form-control py-3 px-4 box pr-10 placeholder-theme-13" value="{{$page_content->page_title}}" placeholder="Title">
            @endif
            <div class="post intro-y overflow-hidden box mt-5">
                <div class="post__tabs nav nav-tabs flex-col sm:flex-row bg-gray-300 dark:bg-dark-2 text-gray-600" role="tablist">
                    <a title="Fill in the article content" data-toggle="tab" data-target="#content" href="javascript:;" class="tooltip w-full sm:w-40 py-4 text-center flex justify-center items-center active" id="content-tab" role="tab" aria-controls="content" aria-selected="true">
                        <i data-feather="file-text" class="w-4 h-4 mr-2"></i> Content
                    </a>
                </div>
                <div class="post__content tab-content">
                    <div id="content" class="tab-pane p-5 active" role="tabpanel" aria-labelledby="content-tab">
                        <div class="border border-gray-200 dark:border-dark-5 rounded-md p-5">
                            <div class="font-medium flex items-center border-b border-gray-200 dark:border-dark-5 pb-5">
                                <i data-feather="chevron-down" class="w-4 h-4 mr-2"></i> Text Content
                            </div>
                            <div class="mt-5">
                            @if(empty($page_content) || is_null($page_content))
                                <textarea name="page_body" id="editor" placeholder="Content of the editor.">
                                </textarea>
                            @else
                            <textarea name="page_body" id="editor" placeholder="Content of the editor.">
                             {{$page_content->page_body}}
                                </textarea>
                            @endif
                            </div>
                        </div>
                        
                    </div>
                </div>
                <div class="intro-y box p-5">
                    <div class="form-check flex-col items-start mt-3">
                        <div class="font-medium flex items-center border-b border-gray-200 dark:border-dark-5 pb-5">
                            <i data-feather="chevron-down" class="w-4 h-4 mr-2"></i>Add Brand Carousel at Bottom
                        </div>
                        @if(empty($page_content) || is_null($page_content))
                        <input id="post-form-5" name="brand" class="form-check-switch" type="checkbox">
                        @else
                            @if(empty($page_content->brand) || is_null($page_content->brand) || $page_content->brand == "off")
                                <input id="post-form-5" name="brand" class="form-check-switch" type="checkbox">
                            @else
                                <input id="post-form-5" name="brand" class="form-check-switch" type="checkbox" checked>    
                            @endif
                        @endif
                    </div>
                </div>
                
                <div class="intro-y box p-5">
                    <div class="form-check flex-col items-start mt-3">
                        <div class="font-medium flex items-center border-b border-gray-200 dark:border-dark-5 pb-5">
                            <i data-feather="chevron-down" class="w-4 h-4 mr-2"></i>Add Testimonials at Bottom
                        </div>
                        @if(empty($page_content) || is_null($page_content))
                        <input id="post-form-5" name="testimonial" class="form-check-switch" type="checkbox">
                        @else
                            @if(empty($page_content->testimonial) || is_null($page_content->testimonial) || $page_content->testimonial == "off")
                            <input id="post-form-5" name="testimonial" class="form-check-switch" type="checkbox" >
                            @else
                            <input id="post-form-5" name="testimonial" class="form-check-switch" type="checkbox" checked>
                            @endif
                        @endif
                    </div>
                </div>
                
                <div class="intro-y box p-5">
                    <div class="form-check flex-col items-start mt-3">
                        <div class="font-medium flex items-center border-b border-gray-200 dark:border-dark-5 pb-5">
                            <i data-feather="chevron-down" class="w-4 h-4 mr-2"></i> Publish
                        </div>
                        @if(empty($page_content) || is_null($page_content))
                        <input id="post-form-5" name="status" class="form-check-switch" type="checkbox">
                        @else
                            @if(empty($page_content->status) || is_null($page_content->status) || $page_content->status == "off")
                            <input id="post-form-5" name="status" class="form-check-switch" type="checkbox">
                            @else
                            <input id="post-form-5" name="status" class="form-check-switch" type="checkbox" checked>
                            @endif
                        @endif
                    </div>
                </div>

                <div class="w-full sm:w-auto flex mt-4 sm:mt-0 p-5">
                    <div class="dropdown">
                    @if(empty($page_content) || is_null($page_content))
                        <button class="btn btn-primary shadow-md flex items-center" aria-expanded="false">
                            Save 
                        </button>
                    @else
                    <button class="btn btn-warning shadow-md flex items-center" aria-expanded="false">
                           Update
                        </button>
                    @endif
                    </div>
                </div>
            </div>
        </div>
        <!-- END: Post Content -->
    </div>
    </form> 

<script>
	initSample();
</script>

@endsection