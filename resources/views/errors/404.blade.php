@extends('layout.layout')
    @section('content')
    <div id="content" class="site-content">
        
        <div class="container">
            <div class="center-text pt-50 not-found text-center">
                <h2><img class="error-image" src="images/404-error.png" alt="404"></h2>
                <h1>Sorry! Page Not Found!</h1>
                <div class="content-404">
                    <p>Oops! The page you are looking for does not exist. Please return to the site is homepage.</p>
                    <a class="octf-btn" href="/">Take Me Home</a>
                </div>
            </div>
        </div>
        
    </div>
    @endsection