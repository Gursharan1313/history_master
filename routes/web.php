<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\PageController;
use App\Http\Controllers\deleteController;
use App\Http\Controllers\postDataController;
use App\Http\Controllers\routeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/',[routeController::class,'index']);

//Route::get('/{id}', [routeController::class,'service'])->name('/');

Route::get('/portfolio', function () {
    return view('sapfiori');
});
Route::get('/case_study', function () {
    return view('case_study');
});
Route::get('/contact', function () {
    return view('web/contact');
});

Route::middleware('loggedin')->group(function() {
    Route::get('login', [AuthController::class, 'loginView'])->name('login-view');
    Route::post('login', [AuthController::class, 'login'])->name('login');
    Route::get('register', [AuthController::class, 'registerView'])->name('register-view');
    Route::post('register', [AuthController::class, 'register'])->name('register');
});


Route::middleware('auth')->group(function() {
    Route::get('logout', [AuthController::class, 'logout'])->name('logout');

    Route::get('/hih-admin', [PageController::class, 'dashboardOverview1'])->name('dashboard-overview-1');
    Route::get('menu', [PageController::class, 'menu'])->name('menu');
    Route::get('pages', [PageController::class, 'menu'])->name('pages');
    Route::get('siteInfo', [PageController::class, 'siteInfo'])->name('siteInfo');
    Route::get('otherpage', [PageController::class, 'otherpage'])->name('otherpage');
    //Route::get('homepage', [PageController::class, 'homepage'])->name('homepage');
    Route::get('post', [PageController::class, 'blog'])->name('blog');
    Route::get('otherpage/{page_name}', [PageController::class, 'otherpage1'])->name('otherpage/');
    Route::get('tag_cate', [PageController::class, 'tabulator'])->name('tag_cate');
    Route::get('file-manager-page', [PageController::class, 'fileManager'])->name('file-manager');
    Route::get('post-page', [PageController::class, 'post'])->name('post');
    Route::get('tours', [PageController::class, 'view_tour'])->name('tours');
    Route::get('tours-post', [PageController::class, 'tour'])->name('tours-post');
    Route::get('resetPass', [PageController::class, 'changePassword'])->name('resetPass');
    Route::get('editTour/{id}', [PageController::class, 'editTour'])->name('editTour/{id}');


    ////Post data route & contoller
    Route::post('category/submit', [postDataController::class, 'category'])->name('category/submit');
    Route::post('file-upload', [postDataController::class, 'file_upload']);
    Route::post('post-blog-data', [postDataController::class, 'blogPost']);
    Route::post('siteInfoData', [postDataController::class, 'siteInfoData']);
    Route::post('post-othePage-data', [postDataController::class, 'othePageData']);
    Route::post('otherpage/post-othePage-data', [postDataController::class, 'othePageDataUpdate']);
    Route::post('addMenu', [postDataController::class, 'addMenu']);
    Route::post('updateMenu', [postDataController::class, 'updateMenu']);
    Route::post('post-tour-data', [postDataController::class, 'postTour']);
    Route::post('checkpass', [postDataController::class, 'checkpass']);
    Route::post('changepassword', [postDataController::class, 'changepassword']);
    
    
    ////delete route & contoller
    Route::post('delete_img', [deleteController::class, 'deleteImg']);
    Route::post('delete_blog', [deleteController::class, 'deleteBlog']);
    Route::post('delete_Page', [deleteController::class, 'deletePage']);
    Route::post('delete_menu', [deleteController::class, 'deleteMenu']);
    Route::post('delete_cat', [deleteController::class, 'delete_cat']);


    

    Route::get('blog-layout-1-page', [PageController::class, 'blogLayout1'])->name('blog-layout-1');

    Route::get('login-page', [PageController::class, 'login'])->name('login');
    Route::get('register-page', [PageController::class, 'register'])->name('register');
    Route::get('error-page-page', [PageController::class, 'errorPage'])->name('error-page');

});
