
        <div class="padding-half bg-light-2">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="partners">
                            <div class="owl-carousel owl-theme home-client-carousel">

                                <div class="partners-slide">
                                    <a href="#" class="client-logo">
                                        <figure class="partners-slide-inner">
                                            <img class="partners-slide-image" src="images/carosel/c1.png" alt="">
                                        </figure>                             
                                    </a>
                                </div>
                                <div class="partners-slide">
                                    <a href="#" class="client-logo">
                                        <figure class="partners-slide-inner">
                                            <img class="partners-slide-image" src="images/carosel/c2.png" alt="">
                                        </figure>                             
                                    </a>
                                </div>
                                <div class="partners-slide">
                                    <a href="#" class="client-logo">
                                        <figure class="partners-slide-inner">
                                            <img class="partners-slide-image" src="images/carosel/c3.png" alt="">
                                        </figure>                             
                                    </a>
                                </div>
                                <div class="partners-slide">
                                    <a href="#" class="client-logo">
                                        <figure class="partners-slide-inner">
                                            <img class="partners-slide-image" src="images/carosel/c4.png" alt="">
                                        </figure>                             
                                    </a>
                                </div>
                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <footer id="site-footer" class="site-footer footer-v1">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                    <div class="widget-footer">
                        <h5 class="text-white">Services</h5>
                        <ul class="list-items">
                            <li class="list-item"><a href="#">SAP Fiori</a></li>
                            <li class="list-item"><a href="#">Blockchain</a></li>
                            <li class="list-item"><a href="#">Mobile App Development</a></li>
                            <li class="list-item"><a href="#">SAP Lumira</a></li>
                            <li class="list-item"><a href="#">IT Staff Augmentation</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                    <div class="widget-footer">
                        <h5 class="text-white">IT Solutions</h5>
                        <ul class="list-items">
                            <li class="list-item"><a href="#">CLoud Solutions</a></li>
                            <li class="list-item"><a href="#">CRM Solutions</a></li>
                            <li class="list-item"><a href="#">SRM Solutions</a></li>
                            <li class="list-item"><a href="#">Enterprise Management Solution</a></li>
                            <li class="list-item"><a href="#">Rapid Deployment Solutions</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                    <div class="widget-footer">
                        <h5 class="text-white">Company</h5>
                        <ul class="list-items">
                            <li class="list-item"><a href="#">About Company</a></li>
                            <li class="list-item"><a href="#">For Customers</a></li>
                            <li class="list-item"><a href="#">Blog & News</a></li>
                            <li class="list-item"><a href="#">Careers & Reviews</a></li>
                            <li class="list-item"><a href="#">Sitemap</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                    <div class="widget-footer">
                        <h5 class="text-white">Subscribe</h5>
                        <p>Follow our newsletter to stay updated  about agency.</p>
                        <form action="newsletter.php" id="mc4wp-form-1" class="mc4wp-form" method="post">
                            <div class="mc4wp-form-fields">
                                <div class="subscribe-inner-form">
                                    <input type="email" name="email" placeholder="Your Email" required="">
                                    <button type="submit" class="subscribe-btn-icon"><i class="flaticon-telegram"></i></button>
                                </div>  
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="row mt-35">
                <div class="col-md-4 mb-4 mb-md-0">
                    <img src="images/logo.png" width="150px" alt="" class="lazyloaded" data-ll-status="loaded">
                </div>
                <div class="col-md-8 text-left text-md-right align-self-center">
                    <p class="copyright-text">Copyright © 2021 Digital Persona by <a href="http://designforrank.com" target="_blank">Design For Rank</a>. All Rights Reserved.</p>
                </div>
            </div>
        </div>
    </footer><!-- #site-footer -->
</div><!-- #page -->
<a id="back-to-top" href="#" class="show"><i class="flaticon-up-arrow"></i></a>
        <!-- jQuery -->
    <script src="js/jquery.min.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/jquery.isotope.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/easypiechart.min.js"></script>
    <script src="js/jquery.countdown.min.js"></script>
    <script src="js/scripts.js"></script>
    <script src="js/header-mobile.js"></script>
    <script src="js/royal_preloader.min.js"></script>
    <script>
        window.jQuery = window.$ = jQuery;  
        (function($) { "use strict";
            //Preloader
            Royal_Preloader.config({
                mode           : 'logo',
                logo           : 'images/logo.svg',
                logo_size      : [160, 75],
                showProgress   : true,
                showPercentage : true,
                text_colour: '#000000',
                background:  '#ffffff'
            });
        })(jQuery);
    </script> 

   <script src="plugins/aos-master/dist/aos.js"></script>
    <script>
      AOS.init({
        easing: 'ease-in-out-sine'
      });
    </script>
</body>
</html>