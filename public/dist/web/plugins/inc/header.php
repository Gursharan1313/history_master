<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <title>Digital Persona | One For Complete Software Solutions</title>
    <link rel="shortcut icon" type="image/x-icon" href="images/favicon.png" />
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <link rel="stylesheet" href="css/font-awesome.min.css" />
    <link rel="stylesheet" href="css/flaticon.css" />
    <link rel="stylesheet" href="css/flaticon2.css" />
    <link rel="stylesheet" href="css/owl.carousel.min.css" />
    <link rel="stylesheet" href="css/owl.theme.css" />
    <link rel="stylesheet" href="css/magnific-popup.css" />
    <link rel="stylesheet" href="plugins/aos-master/dist/aos.css" />
    <link rel="stylesheet" href="style.css" />
    <link rel="stylesheet" href="css/royal-preload.css" />
</head>

<body class="royal_preloader">
    <div id="page" class="site">
        <header id="site-header" class="site-header sticky-header header-static">
            <!-- Main Header start -->
            <div class="header-topbar style-2">
                <div class="octf-area-wrap">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4">
                                <ul class="social-list">
                                    <li><a href="http://twitter.com" target="_self"><i class="fab fa-twitter"></i></a></li>
                                    <li><a href="http://facebook.com" target="_self"><i class="fab fa-facebook-f"></i></a></li>
                                    <li><a href="http://linkedin.com" target="_self"><i class="fab fa-linkedin-in"></i></a></li>
                                    <li><a href="http://instagram" target="_self"><i class="fab fa-instagram"></i></a></li>
                                </ul>
                            </div>
                            <div class="col-md-8">
                                <ul class="topbar-info align-self-end clearfix">
                                    <li><a href="tel:040-4018-1212"><i class="fas fa-phone-alt"></i> 040-4018-1212</a></li>
                                    <li><a href="mailto:info@digitalpersonas.com"><i class="fas fa-envelope"></i> info@digitalpersonas.com</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="octf-main-header">
                <div class="octf-area-wrap">
                    <div class="container octf-mainbar-container">
                        <div class="octf-mainbar">
                            <div class="octf-mainbar-row octf-row">
                                <div class="octf-col logo-col">
                                    <div id="site-logo" class="site-logo">
                                        <a href="index.php">
                                            <img src="images/logo.svg" alt="Engitech" class="">
                                        </a>
                                    </div>
                                </div>
                                <div class="octf-col menu-col">
                                    <nav id="site-navigation" class="main-navigation">
                                        <ul class="menu">
                                            <li class="current-menu-item current-menu-ancestor">
                                                <a href="index.php">Home</a>
                                            </li>
                                            <li class="menu-item-has-children"><a href="#">Services</a>
                                            <ul id="sap" class="sub-menu">
                                                    <li class="menu-item-has-children"><a href="#">SAP Services</a>
                                                        <ul class="sub-menu">
                                                            <li><a href="sapfoiri.php">SAP Fiori</a></li>
                                                            <li><a href="#">SAP HANA</a></li>
                                                            <li><a href="#">SAP Cloud</a></li>
                                                            <li><a href="#">SAP Implementations</a></li>
                                                            <li><a href="#">Enterprise Mobility 		</a></li>
                                                        </ul>
                                                    </li>
                                                    <li class="menu-item-has-children"><a href="#">Hybrid/Web Mobile Development</a>
                                                        <ul class="sub-menu">
                                                            <li><a href="#">Native Development</a></li>
                                                            <li><a href="#">Hybrid Development</a></li>
                                                            <li><a href="#">Design Thinking & Strategy</a></li>
                                                            <li><a href="#">Progressive Web Development</a></li>
                                                            <li><a href="#">Full Stack Development</a></li>
                                                            <li><a href="#">Enterprise Mobile App Development</a></li>
                                                            <li><a href="#">UX Design services</a></li>
                                                        </ul>
                                                    </li>
                                                    <li><a href="#">Strategic Consulting Services</a></li>
                                                    <li><a href="staff_augmentation.php">Staff Augmentation</a></li>
                                                </ul>
                                            </li>
                                            <li class="menu-item-has-children"><a href="#">Solutions</a>
                                                <ul class="sub-menu">
                                                    <li><a href="#">HR & Recruitment</a></li>
                                                    <li><a href="#">Digital Gaming</a></li>
                                                    <li><a href="#">Digital ERP</a></li>
                                                    <li><a href="#">SAP 100+ Custom Fiori Apps</a></li>
                                                </ul>
                                            </li>
                                            <li class="menu-item-has-children"><a href="#">Technology</a>
                                            <ul class="sub-menu">
                                                    <li><a href="portfolio.php">All in Our ppt image</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="case_study.php">Case Studies</a></li>
                                            <li><a href="contact.php">Reach Us</a></li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="header_mobile">
                <div class="container">
                    <div class="mlogo_wrapper clearfix">
                        <div class="mobile_logo">
                            <a href="index.html">
                                <img src="images/logo.svg" alt="Engitech">
                            </a>
                        </div>
                        <div id="mmenu_toggle">
                            <button></button>
                        </div>
                    </div>
                    <div class="mmenu_wrapper">
                        <div class="mobile_nav collapse">
                            <ul id="menu-main-menu" class="mobile_mainmenu">
                            <li class="current-menu-item current-menu-ancestor">
                                                <a href="index.php">Home</a>
                                            </li>
                                            <li class="menu-item-has-children"><a href="#">Services</a>
                                            <ul id="sap" class="sub-menu">
                                                    <li class="menu-item-has-children"><a href="#">SAP Services</a>
                                                        <ul class="sub-menu">
                                                            <li><a href="sapfoiri.php">SAP Fiori</a></li>
                                                            <li><a href="#">SAP HANA</a></li>
                                                            <li><a href="#">SAP Cloud</a></li>
                                                            <li><a href="#">SAP Implementations</a></li>
                                                            <li><a href="#">Enterprise Mobility 		</a></li>
                                                        </ul>
                                                    </li>
                                                    <li class="menu-item-has-children"><a href="#">Hybrid/Web Mobile Development</a>
                                                        <ul class="sub-menu">
                                                            <li><a href="#">Native Development</a></li>
                                                            <li><a href="#">Hybrid Development</a></li>
                                                            <li><a href="#">Design Thinking & Strategy</a></li>
                                                            <li><a href="#">Progressive Web Development</a></li>
                                                            <li><a href="#">Full Stack Development</a></li>
                                                            <li><a href="#">Enterprise Mobile App Development</a></li>
                                                            <li><a href="#">UX Design services</a></li>
                                                        </ul>
                                                    </li>
                                                    <li><a href="#">Strategic Consulting Services</a></li>
                                                </ul>
                                            </li>
                                            <li class="menu-item-has-children"><a href="#">Solutions</a>
                                                <ul class="sub-menu">
                                                    <li><a href="#">HR & Recruitment</a></li>
                                                    <li><a href="#">Digital Gaming</a></li>
                                                    <li><a href="#">Digital ERP</a></li>
                                                    <li><a href="#">SAP 100+ Custom Fiori Apps</a></li>
                                                </ul>
                                            </li>
                                            <li class="menu-item-has-children"><a href="#">technology</a>
                                            <ul class="sub-menu">
                                                    <li><a href="portfolio.php">All in Our ppt image</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="#">Case Studies</a></li>
                                            <li><a href="contact.php">Reach Us</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </header>