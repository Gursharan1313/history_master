
(function( $ ) {
    "use strict";
    function ajx(param){
        var data = $('#'+param).serialize();
        var url = $('#'+param).attr('action');
        $.ajax({
            method:"post",
            url:url,
            data:data,
            success: function(result){
               if(result=="success"){
                   alert('Successfully Updated');
                   location.reload();
               }
               else{
                alert(result);
               }
            }
        });
       
    }

    function delajx(url,data_id){
       
            $.ajax({
                method:"post",
                url:url,
                data:{'id':data_id},
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(result){
                   if(result=="Success"){
                       alert('Successfully Deteled');
                       location.reload();
                   }
                   else{
                    alert(result);
                   }
                }
            });
        
    }   


    var clone_it = ' <div class="itinerary"> <div><label for="post-form-7" id="day1" class="day form-label mt-10 mb-5 ">Day 1</label></div> <div><label for="post-form-7" class="form-label">Itinerary Heading</label> <input id="post-form-7" type="text" name="caption" class="iHead form-control" placeholder="Itinerary Heading"> </div>    <div class="mt-3">        <label class="form-label">Image</label>            <div class="border-2 border-dashed dark:border-dark-5 rounded-md">                <div class="flex flex-wrap px-4">                    <div class="w-24 h-24 relative image-fit mb-5 mr-5 cursor-pointer zoom-in">                        <img id="postImg" class="rounded-md get_img" alt="title image" src="">                        <div id="vid_show">                        </div>                        <input name="file_name" class="iImage" type="hidden" id="file_post">                        <div title="Remove this image?" class="remove tooltip w-5 h-5 flex items-center justify-center absolute rounded-full text-white bg-theme-6 right-0 top-0 -mr-2 -mt-2">                            <i data-feather="x" class="w-4 h-4"></i>                        </div>                    </div>                </div>                <div class="px-4  flex items-center cursor-pointer relative up_img_link">                    <i data-feather="image" class="w-4 h-4 mr-2"></i> <span class="text-theme-1 mr-1">Click to choose</span> Image from media                </div>            </div>        </div>    <div class="mt-5">        <textarea name="body" class="editor" placeholder="Content of the editor.">        </textarea>    </div></div>';

    // $('.postData').click(function () {
    //     var form = $(this).attr('data-id');
    //     setTimeout(function(){
    //         ajx(form);
    //     },1000);
            
    //   });

    $('.popup').click(function(){
        var id= $(this).attr('data-id');
        var anima= $(this).attr('data-animate');
        if(anima ==='fade'){
            $('#'+id).fadeToggle();
        }
        else{
            $('#'+id).slideToggle();
        }
        
    });

    $('.close').click(function(){
        var anima= $(this).attr('data-animate');
        if(anima ==='fade'){
            $(this).parent().fadeOut();
        }
        else{
            $(this).parent().slideUp();
        }
    });

    $('.addNewCate').click(function(){
        $('#submit').show();
        $('#update').hide();
    });

    $('.cateEdit').click(function(){
        $('#addNewCate').slideUp();
        $('#addNewCate').slideDown();
        var name = $(this).attr('data-name');
        var type = $(this).attr('data-type');
        var st = $(this).attr('data-st');
        //console.log(name+type+st);

        $('#catename').val(name);
        $('#catetype').val(type);
        $('#catestatus').val(st);
        $('#submit').hide();
        $('#update').show();

    })

    $('.DelImg').click(function(){
        var img_id = $(this).attr('data-id');
        var con = confirm("Sure wanna delete ?");
        if(con===true){
            delajx('delete_img',img_id);
        }
    });

    $('.delBlog').click(function(){
        var blog_id = $(this).attr('data-id');
        var con = confirm("Sure wanna delete ?");
        if(con===true){
            delajx('delete_blog',blog_id);
        }
    }); 
    $('.cateDel').click(function(){
        var blog_id = $(this).attr('data-id');
        var con = confirm("Sure wanna delete ?");
        if(con===true){
            delajx('delete_cat',blog_id);
        }
    });

    $('.delPage').click(function(){
        var page_id = $(this).attr('data-id');
        var con = confirm("Sure wanna delete ?");
        if(con===true){
            delajx('/delete_Page',page_id);
        }
    });
    $('.delMenu').click(function(){
        var page_id = $(this).attr('data-id');
        var con = confirm("Sure wanna delete ?");
        if(con===true){
            delajx('/delete_menu',page_id);
        }
    });

    $('.form').submit(function(event){
        event.preventDefault();
        //var data = $(this).serialize();
        var id = $(this).attr('id');
        var con = confirm('Sure to submit data');
            if(con==true){
                ajx(id);
            }
    });

    var data_id ;
  
    open_close ();

    $('.sel_image .file').click(function(event){
        event.preventDefault();
       var type=  $(this).find('a').attr('data-type');
        if(type==="image"){
            var img=  $(this).find('a').attr('href');
            if(data_id === undefined){
                $('.get_img').attr('src',img);
                $('#file_post').val(img);
                
            }
            else{
                $('#'+data_id+'_img').attr('src',img);
                $('#'+data_id).val(img);
            }
            $('.get_img').show();
            $('.remove').show();
            $('.sel_image').fadeOut();
        }
        else if(type==="video"){
            alert('Video is not an valid option for now');
        }
        else{
            alert('Video is not an valid option');
        }
            
    })


    $('.textareaPro').keyup(function (e) { 
      var name = $(this).attr('name');
      $(this).attr('name','');
      $(this).hide();
      //alert(name);
      $(this).siblings('textarea').attr('name',name);
      $('.textDiv .ck-editor').show();
     });

     $('#old-pass').change(function (e) { 
         e.preventDefault();
         var pass = $(this).val();
         if(pass.length < 1){}
         else{
                $.ajax({
                    method:"post",
                    url:'checkpass',
                    data:{'oldPass':pass},
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(result){
                    if(result!="hashExist"){
                            $('.changepass').hide();
                            alert('Password Does Not Exist')   
                    }
                    else if(result =="hashExist"){
                            $('.changepass').show();
                    }
                    }
                });
        }

     });


     $('#changepass').submit(function (event) {
        event.preventDefault();
        var pass, confimpass, confrm, form_id;
            pass = $('#change-password-form-1').val();
            confimpass = $('#change-password-form-2').val();
            if(pass === confimpass){
                confrm = confirm('Are you sure to change password');
                if(confrm == true){
                    form_id = $(this).attr('id');
                    ajx(form_id);
                }
            }
            else{
                alert('Password Does Not Matched');
            }

        });


    $('#addNewIt').click(function(event){
        event.preventDefault(); 
        var no = $(this).data('id'); 
         no++;
        var clone_it = ' <div class="itinerary"> <div><label for="post-form-7-'+no+'" id="day'+no+'" class="day form-label mt-10 mb-5 ">Day '+no+'</label></div> <div><label for="post-form-7-'+no+'" class="form-label">Itinerary Heading</label> <input id="post-form-7-'+no+'" type="text" name="iheading[]" class="iHead form-control" required placeholder="Itinerary Heading"> </div>    <div class="mt-3">        <label class="form-label">Image</label>            <div class="border-2 border-dashed dark:border-dark-5 rounded-md">                <div class="flex flex-wrap px-4">                    <div class="w-24 h-24 relative image-fit mb-5 mr-5 cursor-pointer zoom-in">                        <img id="it_'+no+'_img" class="rounded-md get_img" alt="title image" src="">                        <div id="vid_show">                        </div>                        <input name="iImg[]" class="form-control" type="hidden" id="it_'+no+'">                        <div title="Remove this image?" class="remove tooltip w-5 h-5 flex items-center justify-center absolute rounded-full text-white bg-theme-6 right-0 top-0 -mr-2 -mt-2">                            <i data-feather="x" class="w-4 h-4"></i>                        </div>                    </div>                </div>                <div data-id="it_'+no+'" class="px-4  flex items-center cursor-pointer relative up_img_link">                    <i data-feather="image" class="w-4 h-4 mr-2"></i> <span class="text-theme-1 mr-1">Click to choose</span> Image from media                </div>            </div>        </div>    <div class="mt-5">        <label class="form-label">Itinerary Content</label>     <textarea name="iContent[]"class="form-control" placeholder="Content of the editor." required> </textarea>   </div></div>';
        $('#itinerary').append(clone_it);

        $(this).data('id',no);

        open_close ();
       
    });

    function open_close (){
         
         $('.up_img_link').click(function(){
            $('.sel_image').fadeIn();
            data_id = $(this).attr('data-id');
        });
        $('.remove').click(function(){
            $(this).hide();
            $(this).siblings().hide();
            $(this).siblings().val('');
        });
    }

})( jQuery );