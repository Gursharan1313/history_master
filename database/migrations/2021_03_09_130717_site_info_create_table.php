<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SiteInfoCreateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('site_info', function (Blueprint $table) {
            $table->id();
            $table->text('website_title');
            $table->string('logo',200);
            $table->string('favicon',200)->nullable();
            $table->string('add',500);
            $table->string('email',50);
            $table->bigInteger('phone');
            $table->string('gmap',500)->nullable();
            $table->string('facebook',200)->nullable();
            $table->string('linkedin',200)->nullable();
            $table->string('twitter',200)->nullable();
            $table->string('instagram',200)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('site_info');
    }
}
