<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class OtherpageCreatTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('otherPage', function (Blueprint $table) {
            $table->id();
            $table->text('page_title');
            $table->longText('page_body');
            $table->string('href_link',200);
            $table->enum('brand', ['on', 'off'])->default('off');
            $table->enum('testimonial', ['on', 'off'])->default('off');
            $table->enum('status', ['on', 'off'])->default('off');
            $table->text('menu_status');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('otherPage');
    }
}
