<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class HomePageCreatTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('home_page', function (Blueprint $table) {
            $table->id();
            $table->string('background',200);
            $table->string('heading',200);
            $table->string('side_img',500);
            $table->string('video_link',100)->nullable();
            $table->string('tab1',50);
            $table->string('tab2',50);
            $table->string('tab3',50);
            $table->string('brand',1000);
            $table->string('abt_img',200);
            $table->string('abt_content',5000);
            $table->string('htitle',200);
            $table->string('hheading',200);
            $table->string('hbox1',500);
            $table->string('hbox2',500);
            $table->string('hbox3',500);
            $table->string('hbox4',500);
            $table->string('testimonial',500)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('home_page');
    }
}
