<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->id();
            $table->text('title');
            $table->text('href_link')->nullable();
            $table->text('body');
            $table->string('file_name',250);
            $table->text('caption',100)->nullable();
            $table->text('written_by');
            $table->date('post_date');
            $table->string('categories',250);
            $table->string('tags',250);
            $table->enum('status', ['on', 'off']);
            $table->enum('show_author', ['on', 'off']);
            $table->string('show_comment',20)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post');
    }
}
