<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class site_info extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = "site_info";
 
    protected $fillable = ['website_title','logo','favicon','add','email','phone','gmap','facebook','linkedin','twitter','instagram'];

}
