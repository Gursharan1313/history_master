<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class tour extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = "tour";
    protected $fillable = ['tour_name','overview','location','bgImg','status'];

}
