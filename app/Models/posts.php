<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class posts extends Model
{
    use HasFactory;
    use SoftDeletes;

 
    protected $fillable = ['title','href_link','body','file_name','caption','written_by','post_date','categories','tags','status','show_author'];

    public function getDateAttribute($value)
    {
        return Carbon::createFromFormat('Y-m-d', $value)->format('d/m/Y');
    }

    public function setDateAttribute($value)
    {
        $this->attributes['post_date'] = Carbon::createFromFormat('d/m/Y', $value)->toDateString();
    }



}
