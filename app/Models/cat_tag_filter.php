<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class cat_tag_filter extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = "cat_tag_filter";
    protected $fillable = ['cat_tag_id','parent_id','type','parent_type'];
}
