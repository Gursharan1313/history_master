<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class otherPage extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = "otherPage";
 
    protected $fillable = ['page_title','page_body','href_link','brand','testimonial','status'];
}
