<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class itinerary extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = "itinerary";
    protected $fillable = ['parent_id','iheading','iImg','iContent'];
}
