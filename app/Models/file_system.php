<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class file_system extends Model
{
    use HasFactory;

    use SoftDeletes;

    protected $fillable = ['file_name','file_type','location','file_size'];
}
