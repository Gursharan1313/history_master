<?php

namespace App\Http\Controllers;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

use App\models\file_system;
use App\models\category;
use App\models\posts;
use App\models\otherPage;
use App\models\menu;

use Illuminate\Support\Facades\File;
use App\Http\Requests\StorePostRequest;

use Illuminate\Http\Request;

class deleteController extends Controller
{
   /**
     * Delete images.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function deleteImg(Request $request)
    {
        $id = $request->post('id');
        
        $file = file_system::all()->where('id',$id)->pluck('file_name');
            foreach($file as $data){
                $name = $data;
            }
           if(Storage::delete($name)){
                $files = file_system::findOrFail($id);
                $files->forceDelete();
                echo 'Success';
           }
           else{
               echo "Fail to delete";
           }
    }
    /**
     * Delete BLogs.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function deleteBlog(Request $request)
    {   
        $id = $request->post('id');
        $files = posts::findOrFail($id);
        if($files->delete()==true){
            echo 'Success';
        }
        else{
            echo 'fail';
        }
        
    }
  
    /**
     * Delete BLogs.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function delete_cat(Request $request)
    {   
        $id = $request->post('id');
        $files = category::findOrFail($id);
        if($files->delete()==true){
            echo 'Success';
        }
        else{
            echo 'fail';
        }
        
    }


    /**
     * Delete BLogs.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function deletePage(Request $request)
    {   
        $id = $request->post('id');
        $files = otherPage::findOrFail($id);
        if($files->forceDelete()==true){
            echo 'Success';
        }
        else{
            echo 'fail';
        }
        
    } 
    
    /**
     * Delete BLogs.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function deleteMenu(Request $request)
    {   
        $id = $request->post('id');
        $files = menu::findOrFail($id);

        $href = $files->href_link;

        otherPage::where('href_link',$href)->update(array('menu_status'=>null));

        if($files->forceDelete()==true){
            echo 'Success';
        }
        else{
            echo 'fail';
        }
       
    }

}
