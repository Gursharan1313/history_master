<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use App\models\file_system;
use App\models\category;
use App\models\posts;
use App\models\menu;
use App\models\site_info;
use App\models\otherPage;
use App\models\User;
use App\models\tour;
use App\models\cat_tag_filter;
use App\models\itinerary;

use Illuminate\Support\Facades\File;
use App\Http\Requests\StorePostRequest;

class PageController extends Controller
{
    /**
     * Show specified view.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function dashboardOverview1()
    {
        return view('pages/dashboard',['pageName' => 'Home']);
    }

    /**
     * Show specified view.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function menu()
    {
        $menu_items = otherPage::where('menu_status','!=','enable')->orWhereNull('menu_status')->get();
        $menu = menu::where('type','menu')->get();
        return view('pages/menu',compact('menu_items','menu'),[ 'pageName' => 'Menu']);
    }

     /**
     * Show specified view.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function fileManager()
    {
        $file = file_system::all();
        return view('pages/file-manager',compact('file'),['pageName' => 'File Manager']);
    }
    
    /**
     * Show specified view.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function tour()
    {
        $cat = category::where('type','category')->get();
        $tag = category::where('type','tag')->get();
        $file = file_system::all();
        return view('pages/tour',['pageName' => 'Add Tour'],compact('cat','tag','file'));
    }

    public function post()
    {
        $cat = category::where('type','category')->get();
        $tag = category::where('type','tag')->get();
        $file = file_system::all();
        return view('pages/post',['pageName' => 'Add Post'],compact('cat','tag','file'));
    }
    
    
    /**
     * Show specified view.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function blog()
    {   
        //$file = file_system::all();
        $post = posts::orderBy('created_at','desc')->get();
        return view('pages/blog',compact('post'),['pageName' => 'Post']);
    }

  

     /**
     * Show specified view.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function tabulator()
    {
        $cat = category::all();
        return view('pages/tabulator',compact('cat'),['pageName' => 'Categories & Tags']);
    }


    /**
     * Show specified view.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function siteInfo()
    {   
        $data = site_info::get()->last();
        $file = file_system::all();
        return view('pages/siteInfo',['pageName' => 'Site Info'],compact('file','data'));
    }
    
    /**
     * Show specified view.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function homepage()
    {   
        $data = site_info::get()->last();
        $file = file_system::all();
        return view('pages/homepage',['pageName' => 'Home Page'],compact('file','data'));
    }
    
    public function otherpage()
    {   
        $message = "";
        $pages = otherPage::all();
        $page_content = " "; 
        return view('pages/otherPage',['pageName' => 'Other Pages'],compact('pages'));
    }

    public function otherpage1($request)
    {   
        $page_content = otherPage::find($request);
        $message = "";
        $pages = otherPage::all();
        return view('pages/otherPage',['pageName' => 'Other Pages'],compact('pages','page_content'));
    }
  
    /**
     * Show specified view.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function login()
    {
        return view('pages/login');
    }

    /**
     * Show specified view.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register()
    {
        return view('pages/register');
    }

    /**
     * Show specified view.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function errorPage()
    {
        return view('pages/error-page');
    }

    /**
     * Show specified view.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updateProfile()
    {
        return view('pages/update-profile');
    }

    /**
     * Show specified view.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function changePassword()
    {   
        $id = User::all()->pluck('email');
        return view('pages/change-password',compact('id'),['pageName' => 'Change Password']);
    }
  
    /**
     * Show specified view.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function view_tour()
    {   
        $tour = tour::orderBy('created_at','desc')->get();
        return view('pages/viewTour',compact('tour'),['pageName' => 'View/ Edit Tour']);
    }
 
    /**
     * Show specified view.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function editTour($request)
    {   
        $id = $request;
        $tour = tour::findOrFail($id);
        $cat = cat_tag_filter::where('parent_id',$tour->id)
                ->where('type','category')
                ->where('parent_type','tour')
                ->get();
        $tag = cat_tag_filter::where('parent_id',$tour->id)
                ->where('type','tag')
                ->where('parent_type','tour')
                ->get();
        $it =  itinerary::where('parent_id',$tour->id)->get(); 

        return view('pages/editTour',compact('tour','cat','tag','it'),['pageName' => 'View/ Edit Tour']);
    }

  
}
