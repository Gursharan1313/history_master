<?php

namespace App\Http\Controllers;

use Gate;
use Carbon\Carbon;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Storage;

use App\models\file_system;
use App\models\category;
use App\models\posts;
use App\models\site_info;
use App\models\otherPage;
use App\models\menu;
use App\models\User;
use App\models\tour;
use App\models\cat_tag_filter;
use App\models\itinerary;

use Illuminate\Support\Facades\File;
use App\Http\Requests\StorePostRequest;
use App\Http\Requests\StoreSiteInfoRequest;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class postDataController extends Controller
{
     /**
     * File Upload in Media.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function file_upload(Request $request)
    {

        $ext = $request->file('file')->extension();
        $fszie = $request->file('file')->getSize();  /////// Get File Size in bytes
        $file_szie = number_format($fszie/1000000,1).'mb';  /////// convert it to Mb's with one digit after decimal using number format

        if($ext == 'jpeg' || $ext == 'jpg' || $ext == 'png'){
            $name = $request->file('file')->storePublicly('public/images');
                //Storage::setVisibility($name, 'public');
            $location = "images";
        }
        elseif($ext == 'pdf'){
            $name = $request->file('file')->storePublicly('public/docs');
            $location = "docs";
        }
        else{
            $name = $request->file('file')->storePublicly('public/videos');
            $location = "videos";
        }

        $request->request->add(['file_name' => $name]);
        $request->request->add(['file_size' => $file_szie]);
        $request->request->add(['file_type' => $ext]);
        $request->request->add(['location' => $location ]);

        $requestData = $request->all();
        
        file_system::create($requestData);

        $file = file_system::all()->where('deleted_at',' ');

        return view('pages/file-manager',compact('file'),['pageName' => 'File Manager']);
    }

     /**
     * Post Blog.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function blogPost(StorePostRequest $request)
    {
        $date =carbon::parse($request->post('post_date'))->format('Y-m-d');
        $cat =json_encode($request->post('categories'));
        $tag =json_encode($request->post('tags'));
        $href = str_replace(' ','_',$request->post('title'));
        $request->request->add(['post_date'=>$date,'href_link'=> $href,'categories'=>$cat,'tags'=> $tag]);
        $requestData = $request->all();
        if(posts::create($requestData)==true){
            echo "success";
        }
        else{
            echo "Fail to Submit data";
        }
    }

    /**
     * Add Category.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function category(Request $request)
    {
        $type = $request->post('type');
        if($type !="Select one"){
            $requestData = $request->all();
           if(category::create($requestData)==true){
                echo "success";
            }
            else{
                echo "Fail to Submit data";
            }
        }
        else{
            echo "Please Select Type Before Submit";
        }
         //category::all();
        //return;
    }
    /**
     * Add Category.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function upCategory(Request $request)
    {
        $type = $request->post('type');
        if($type !="Select one"){
            $requestData = $request->all();
           if(category::create($requestData)==true){
                echo "success";
            }
            else{
                echo "Fail to Submit data";
            }
        }
        else{
            echo "Please Select Type Before Submit";
        }
         //category::all();
        //return;
    }
    
    /**
     * Add Site Info.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function siteInfoData(Request $request)
    {
        //echo $request->post('add');

         $requestData = $request->all();
         if(site_info::create($requestData)==true){
             return redirect()->back()->with('msg','Successfully Updated');
         }
         else{
            return redirect()->back()->withErrors('Fail to Submit data'); 
         }
        
       
    } 
    
    /**
     * Add Other Page.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function othePageData(Request $request)
    {   

        $href = str_replace(' ','_',$request->post('page_title'));
        $request->request->add(['href_link'=> $href]);
        $requestData = $request->all();
        if(otherPage::create($requestData)){
            $message = "Successfully Created Page";
            $pages = otherPage::all();
            return view('pages/otherPage',['pageName' => 'Other Pages'],compact('pages','message'));
        }
        else{
            echo "Fail to Submit data";
        } 
       
    }
    
    /**
     * Add Update Other Page.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function othePageDataUpdate(Request $request)
    {   

        $delpage =  otherPage::find($request->post('page_id'));
        if($delpage->delete()==true){
            $href = str_replace(' ','_',$request->post('page_title'));
            $request->request->add(['href_link'=> $href]);
            $requestData = $request->all();
            if(otherPage::create($requestData)){
                $message = "Successfully Updated";
                $pages = otherPage::all();
                return view('pages/otherPage',['pageName' => 'Other Pages'],compact('pages','message'));
            }
            else{
                echo "Fail to Submit data";
            }
        }
        else{
            $message = "Fail to Update";
        }

       
    }
    
    
    /**
     * Add menu.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function addMenu(Request $request)
    {   
        if(empty($request->post('link')) && empty($request->post('menu_name'))){
            echo "Please Select either Page name or Link";
        }
        elseif(empty($request->post('link')) && empty($request->post('rename'))){
            $href = strtolower(str_replace(' ','_',$request->post('menu_name')));
            $request->request->add(['href_link'=> $href]);

        }
         elseif(!empty($request->post('link')) && !empty($request->post('rename'))){
            $href = $request->post('link');
            $menu = $request->post('rename');

            $request->request->add(['href_link'=> $href, 'menu_name'=>$menu]);
        }
        $requestData =$request->all();
        
            if(menu::create($requestData)==true){
                $menuup = otherPage::where('href_link',$href)->update(array('menu_status'=>'enable'));
                echo 'success';
            }
            else{
                echo 'Fail to Create Menu';
            }
        
               
    }
    
    /**
     * Add menu.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updateMenu(Request $request)
    {
        $menuId = $request->post('menu_id');
        if(!empty($request->post('sub-menu'))){
            foreach($request->post('sub-menu') as $sub){
                $href = strtolower(str_replace(' ','_',$sub));
                $request->request->add(['href_link'=> $href,'parent_id'=>$menuId]);
                   
                if(menu::create($requestData)==true){
                    $menuup = otherPage::where('href_link',$href)->update(array('menu_status'=>'enable'));
                    echo 'success';
                }
                else{
                    echo 'Fail to Create Menu';
                }
            } 
        }
          
    }

    /**
     * Add Post Tour.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postTour(Request $request)
    {   
       $this->validate($request,[
        'bgImg'=>'required',
        'iImg' => 'required'
       ]);

         $requestData = $request->all();
            $t = tour::create($requestData);
            $tour_id = $t->id;
            if($t==true){
                $iterc = $request->post('iContent');
                $iterh = $request->post('iheading');
                $iteri = $request->post('iImg');

                for($i=0; $i < count($iterc); $i++ ){
                    $request->request->add(['parent_id' => $tour_id,'iheading'=>$iterh[$i], 'iContent'=>$iterc[$i],'iImg'=>$iteri[$i] ]);
                    $requestData1 = $request->all();
                    $itour = itinerary::create($requestData1);
                }
                if($itour == true){
                    
                    $cate = $request->post('categories');
                    $tag = $request->post('tags');
                    foreach($cate as $cat){
                        $request->request->add(['parent_id' => $tour_id,'cat_tag_id' => $cat,'type'=>'category', 'parent_type'=>'tour' ]);
                         $requestData2 = $request->all();
                        $catdb = cat_tag_filter::create($requestData2);
                    }
                    foreach($tag as $tag){
                        $request->request->add(['parent_id' => $tour_id,'cat_tag_id' => $tag,'type'=>'tag', 'parent_type'=>'tour' ]);
                         $requestData2 = $request->all();
                        $tagdb = cat_tag_filter::create($requestData2);
                    }
                }
               if($tagdb == true && $catdb == true){
                    return redirect()->back()->with('msg','Successfully Updated');
               } 
                
             }
             return redirect()->back()->withErrors('Fail to Update New Tour in DB');
                
    }
    /**
     * Check Old Password Existance.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function checkpass(Request $request)
    {
        
        //$pass = $request->post('id');
        $check = User::get()->last();
        if (Hash::check($request->post('oldPass'), $check->password)) {
            echo 'hashExist';
        }
        else{
            echo 'no';
        }
                  
    }
    public function changepassword(Request $request)
    {
        
       $pass = hash::make( $request->post('password'));
       if(User::where('email',$request->post('id'))->update(['password'=>$pass])){
            echo 'Password has been changed Successfully';
       }
       else{
           echo 'fail to update password';
       }
                  
    }





}
