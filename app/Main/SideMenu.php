<?php

namespace App\Main;



class SideMenu
{

    /**
     * List of side menu items.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public static function menu()
    {
        return [
            'dashboard' => [
                'icon' => 'home',
                'route_name' => 'dashboard-overview-1',
                'title' => 'Dashboard',
                'params' => [
                   
                ]
            ],
            'Tours' => [
                'icon' => 'layout',
                'title' => 'Tours',
                'sub_menu' => [
                    'viewpost' => [
                        'icon' => 'file-text',
                        'title' => 'View/Edit Tour',
                        'route_name' => 'tours',
                        'params' => []
                    ],
                    'addnew' => [
                        'icon' => 'file-text',
                        'title' => 'Add New Tour',
                        'route_name' => 'tours-post',
                        'params' => []
                    ],
                ]
            ],
            'Menu' => [
                'icon' => 'sidebar',
                'route_name' => 'menu',
               'params' => [ ],
                'title' => 'Menu'
            ], 
            'Add/Edit tags & Categories' => [
                'icon' => 'sidebar',
                'title' => 'Add/Edit tags & Categories',
                'route_name' => 'tag_cate',
                'params' => [ ],
            ], 
            'file-manager' => [
                'icon' => 'hard-drive',
                'route_name' => 'file-manager',
               'params' => [ ],
                'title' => 'File Manager'
            ],
            'Blog' => [
                'icon' => 'layout',
                'title' => 'Blog',
                'sub_menu' => [
                    'viewpost' => [
                        'icon' => 'file-text',
                        'title' => 'View/Edit Post',
                        'route_name' => 'blog',
                        'params' => []
                    ],
                    'addnew' => [
                        'icon' => 'file-text',
                        'title' => 'Add New Post',
                        'route_name' => 'post',
                        'params' => []
                    ],
                ]
            ],
            'other_pages' => [
                'icon' => 'layout',
                'title' => 'Other Pages',
                'sub_menu' => [
                    'Site_info' => [
                        'icon' => '',
                        'title' => 'Site Info',
                        'route_name' => 'siteInfo',
                        'params' => []
                    ],
                    'Other_pages' => [
                        'icon' => '',
                        'title' => 'Other Pages',
                        'route_name' => 'otherpage',
                        'params' => []
                    ],
                ]
            ]
        ];
    }
}
