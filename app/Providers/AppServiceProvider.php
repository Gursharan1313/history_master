<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\models\site_info;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {   

        $siteInfo = site_info::get()->last();
        view()->share('siteInfo', $siteInfo);
    }
}
